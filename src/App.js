import logo from './logo.svg';
import './App.css';
import { Route, Routes } from 'react-router-dom';
import LandingPage from './page/LandingPage';
import LoginPage from './page/LoginPage';
import RegisterPage from './page/RegisterPage';
import ListMenuKelasPage from './page/ListMenuKelasPage';
import CheckOutPage from './page/CheckOutPage';
import DetailInvoicePage from './page/DetailInvoicePage';
import EmailConfirmSuccessPage from './page/EmailConfirmSuccessPage';
import InvoicePage from './page/InvoicePage';
import PurchaseSuccessPage from './page/PurchaseSuccessPage';
import ResetPasswordPage from './page/ResetPasswordPage';
import ResetPasswordEmailPage from './page/ResetPasswordEmailPage';
import ResetPasswordNewPassPage from './page/ResetPasswordNewPassPage';
import DetailKelasPage from './page/DetailKelasPage';
import MyClassPage from './page/MyClassPage';
import FormMetodePembayaran from './page/FormMetodePembayaran';
import { AuthProvider } from './Context/AuthContext';


function App() {
  return (
    <div className="App">
      <AuthProvider>
        <Routes>
          <Route path="/" element={<LandingPage />} />
          <Route path="/Login" element={<LoginPage />} />
          <Route path="/Register" element={<RegisterPage />} />
          <Route path="/ListMenuKelas/:id" element={<ListMenuKelasPage />} />
          <Route path="/CheckOut" element={<CheckOutPage />} />
          <Route path="/DetailInvoice/:id" element={<DetailInvoicePage />} />
          <Route path="/DetailKelas/:id" element={<DetailKelasPage />} />
          <Route path="/EmailConfirmSuccess/:email" element={<EmailConfirmSuccessPage />} />
          <Route path="/Invoice" element={<InvoicePage />} />
          <Route path="/MyClass" element={<MyClassPage />} />
          <Route path="/PurchaseSuccess" element={<PurchaseSuccessPage />} />
          <Route path="/ResetPassword" element={<ResetPasswordPage />} />
          <Route path="/ResetPasswordEmail" element={<ResetPasswordEmailPage />} />
          <Route path="/ResetPasswordNewPass/:email" element={<ResetPasswordNewPassPage />} />
          <Route path="/FormMetodePembayaran" element={<FormMetodePembayaran />} />
        </Routes>
      </AuthProvider>
    </div>
  );
}

export default App;
