import { Alert, Button, Card, Dialog, DialogContent, DialogTitle, Grid, List, ListItem, Modal, Paper, Snackbar, Table, TableBody, TableCell, tableCellClasses, TableContainer, TableHead, TableRow, Typography } from '@mui/material'
import { APIRequest as Axios } from '../component/Axios.js';
import React, { useEffect, useState } from 'react'
import NavbarLogin from '../component/NavbarLogin'
import { Box } from '@mui/system';
import styled from '@emotion/styled';
import { useNavigate } from 'react-router-dom';
import jwtDecode from 'jwt-decode';

const FormMetodePembayaran = () => {
  const navigate = useNavigate();

  const getPayloadFromToken = () => {
    const token = localStorage.getItem('token');
    try {
      const decoded = jwtDecode(token);
      return decoded;
    } catch (err) {
      return null;
    }
  };
  const token = getPayloadFromToken();
  const accessToken = localStorage.getItem('token');
  const headers = { Authorization: `Bearer ${accessToken}` };

  useEffect(()=>{
    const checkRole = async() =>{
      if(token!==null){
          if(token.role!=0){
            navigate("/");
          }
      }else{
        navigate("/");
      }
    };
    checkRole();
  },[]);

    const [imageBase64, setImageBase64] = useState("");
    const [paymentMethodName, setPaymentMethodName] = useState("");
    const [listPaymentMethod, setListPaymentMethod] = useState([]);

    const [editId, setEditId] = useState("");
    const [editImageBase64, setEditImageBase64] = useState("");
    const [editPaymentMethodName, setEditPaymentMethodName] = useState("");

    const refresh =() =>window.location.reload(true);
    const [open, setOpen] = useState(false);
    const [openDelete, setOpenDelete] = useState(false);

    const handleClickOpen = (item) => {
      setOpen(true);
      setEditId(item.id);
      setEditPaymentMethodName(item.name);
      setEditImageBase64(item.imageFile);
    };

    const handleClose = () => {
      setOpen(false);
      setEditId("");
      setEditPaymentMethodName("");
      setEditImageBase64("");
    };

    const handleClickDelete = (item) => {
      setOpenDelete(true);
      setEditId(item.id);
      setEditPaymentMethodName(item.name);
      setEditImageBase64(item.imageFile);
    };

    const handleCloseDelete =() =>{
      setOpenDelete(false);
      setEditId("");
      setEditPaymentMethodName("");
      setEditImageBase64("");
    };


    const onImagePick = (e) =>{
        const tempImage = e.target.files[0];
        let reader = new FileReader();

        reader.onload = function(){
          setImageBase64(reader.result);
        };
        reader.readAsDataURL(tempImage);
    };

    const onEditImagePick = (e) =>{
      const tempImage = e.target.files[0];
      let reader = new FileReader();

      reader.onload = function(){
        setEditImageBase64(reader.result);
      };
      reader.readAsDataURL(tempImage);
  };

    const onNameChange = (e) =>{
      const tempString = e.target.value;
      setPaymentMethodName(tempString);
    }

    const onEditNameChange = (e) =>{
      const tempString = e.target.value;
      setEditPaymentMethodName(tempString);
    }

    const saveToDb = async () =>{
        let formData = new FormData();
        formData.append("Id",0);
        formData.append("Name", paymentMethodName);
        formData.append("ImageFile",imageBase64);
        Axios({
          method:"POST",
          url:"PaymentMethod/Insert",
          data: formData,
          headers:{
            "Content-Type":"multipart/form-data",
            "Authorization" : `Bearer ${accessToken}`
          }
        })
        .then((res)=>{
          if(res.status === 200){
            fetchPaymentMethod();
            setImageBase64("");
            document.getElementById("name").value = "";
            document.getElementById("image").value = "";
          }
        })
        .catch((err) =>{
          handleOpenSnackbar();
        })
    }

    const updateToDb = async () =>{
      let formData = new FormData();
      formData.append("Id",editId);
      formData.append("Name", editPaymentMethodName);
      formData.append("ImageFile",editImageBase64);
      Axios({
        method:"PATCH",
        url:"PaymentMethod/Update",
        data: formData,
        headers:{
          "Content-Type":"multipart/form-data",
          "Authorization" : `Bearer ${accessToken}`
        }
      })
      .then((res)=>{
        if(res.status === 200){
          fetchPaymentMethod();
          handleClose();
        }
      })
      .catch((err) =>{
        handleOpenSnackbar();
      })
  };

  const deletePaymentMethod = async () =>{
    Axios({
      method:"Delete",
      url:"PaymentMethod/Delete?id="+editId,
      headers:{
        "Content-Type":"multipart/form-data",
        "Authorization" : `Bearer ${accessToken}`
      }
    })
    .then((res)=>{
      if(res.status === 200){
        fetchPaymentMethod();
        handleCloseDelete();
      }
    })
    .catch((err) =>{
      handleOpenSnackbar();
    })
}
  const fetchPaymentMethod = async() =>{
    try{
      const result = await Axios.get('PaymentMethod/SelectAll',{headers});
    setListPaymentMethod(result.data);
    }catch(err){
      handleOpenSnackbar();
    }
  };

    useEffect(()=>{
      fetchPaymentMethod();
    },[]);

    const StyledTableCell = styled(TableCell)(({ theme }) => ({
      [`&.${tableCellClasses.head}`]: {
        backgroundColor: '#F2C94C',
        color: '#4F4F4F',
      },
      [`&.${tableCellClasses.body}`]: {
        fontSize: 14,
      },
    }));
    
    const StyledTableRow = styled(TableRow)(({ theme }) => ({
      '&:nth-of-type(even)': {
        backgroundColor: '#F2C94C33',
      },
      '&:last-child td, &:last-child th': {
        border: 0,
      },
    }));

    const [openSnackbar, setOpenSnackbar] = useState(false);
    const handleOpenSnackbar = () =>{
      setOpenSnackbar(true);
    }
    const handleCloseSnackbar = () =>{
      setOpenSnackbar(false);
    }

  return (
    <div>
      <NavbarLogin/>
      <Grid container sx={{margin:'auto'}}>
        <Grid item xs={2}></Grid>
        <Grid item xs={8}>
          <Box sx={{margin:"20px 0px 20px 0px",padding:"15px 0px 15px 0px",border:"5px solid #F2C94C",width:'100%'}}>
              <Typography variant='h4' sx={{margin:'auto'}}>
                Form Payment Method
              </Typography>
              <Grid container spacing ={2} sx={{margin:'auto',textAlign:'left'}}>
                <Grid item xs={2}>
                  Name  
                </Grid>
                <Grid item xs={10}>
                  : <input type="text" id="name"onChange={onNameChange}></input>
                </Grid>
                <Grid item xs={2}>
                  Image File  
                </Grid>
                <Grid item xs={10}>
                  : <input type="file" id="image" onChange={onImagePick} accept="image/" />
                </Grid>
                <Grid item xs={2}></Grid>
                <Grid item xs={10}>
                    <img src={imageBase64} alt="img" style={{width:"150px",height:"150px"}} />
                </Grid>
                <Grid item xs={2}></Grid>
                <Grid item xs={10} sx={{margin:'auto'}}>
                <Button sx={{width:150,backgroundColor:'#5D5FEF',color:'white',borderRadius:'8px'}} onClick={saveToDb}>
                  Save
                </Button>
                </Grid>
              </Grid>
          </Box>
      <Box style={{width:'100%', padding:0,border:"5px solid #F2C94C",justifyContent:'center',marginBottom:20}}>
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 700 }} aria-label="customized table">
            <TableHead>
            <TableRow>
                <StyledTableCell align="center">No</StyledTableCell>
                <StyledTableCell align="center">Logo</StyledTableCell>
                <StyledTableCell align="center">Nama</StyledTableCell>
                <StyledTableCell align="center">Edit</StyledTableCell>
                <StyledTableCell align="center">Delete</StyledTableCell>
            </TableRow>
            </TableHead>
            <TableBody>
            {listPaymentMethod.map((row,index) => (
                <StyledTableRow key={index}>
                    <StyledTableCell component="th" scope="row" align="center">
                        {index+1}
                    </StyledTableCell>
                    <StyledTableCell align="center">
                        <img src={row.imageFile} alt="Image not Found" style={{maxHeight:'50px',maxWidth:'50px'}}/>
                    </StyledTableCell>
                    <StyledTableCell align="center">{row.name}</StyledTableCell>
                    <StyledTableCell align="center">
                      <Button onClick={()=>handleClickOpen(row)}>
                        Edit
                      </Button>
                    </StyledTableCell>
                    <StyledTableCell align="center">
                      <Button sx={{color:'red'}} onClick={()=>handleClickDelete(row)}>
                        Delete
                      </Button>
                    </StyledTableCell>
                </StyledTableRow>
            ))}
            </TableBody>
        </Table>
        </TableContainer>
            {/* Edit Payment Method Dialog Box */}
            <Dialog open={open} onClose={handleClose}>
                <Box sx={{alignItems:'center'}}>
                  <DialogTitle sx={{textAlign:'center'}} variant="h6" component="h2">
                    Edit Payment Method
                  </DialogTitle>
                  <DialogContent sx={{textAlign:'center'}}>
                  <Grid container spacing ={2}>
                    <Grid item xs={12}>
                      Name : <input type="text" onChange={onEditNameChange} value={editPaymentMethodName}></input>
                    </Grid>
                    <Grid item xs={12}>
                      <input type="file" onChange={onEditImagePick} accept="image/" />
                    </Grid>
                    <Grid item xs={12}>
                      <img src={editImageBase64} alt="Image not Found" style={{maxWidth:"150px"}} />
                    </Grid>
                    <Grid item xs={12}>
                      <Button style={{color:'green'}} onClick={updateToDb}>Save</Button>
                      <Button style={{color:'blue'}} onClick={handleClose}>Cancel</Button>
                    </Grid>
                  </Grid>
                  </DialogContent>
                </Box>
              </Dialog>

              {/* Delete Confirmation Dialog Box */}
              <Dialog open={openDelete} onClose={handleCloseDelete}>
              <Box sx={{alignItems:'center'}}>
                  <DialogTitle sx={{textAlign:'center'}} variant="h6" component="h2">
                    Delete Confirmation
                  </DialogTitle>
                  <DialogContent sx={{textAlign:'center'}}>
                  <Grid container spacing ={2}>
                    <Grid item xs={12}>
                      Do you want to delete {editPaymentMethodName} ?
                    </Grid>
                    <Grid item xs={12}>
                      <img src={editImageBase64} alt="Image not Found" style={{maxWidth:"150px"}} />
                    </Grid>
                    <Grid item xs={12}>
                      <Button style={{color:'red'}} onClick={deletePaymentMethod}>Delete</Button>
                      <Button style={{color:'blue'}} onClick={handleCloseDelete}>Cancel</Button>
                    </Grid>
                  </Grid>
                  </DialogContent>
                </Box>
              </Dialog>
            </Box>
            </Grid>
            <Grid item xs={2}></Grid>
      </Grid>
      <Snackbar open={openSnackbar} autoHideDuration={6000} onClose={handleCloseSnackbar}>
          <Alert onClose={handleCloseSnackbar} severity="error" sx={{ width: '100%' }}>
              Unauthorize Access!
          </Alert>
      </Snackbar>
    </div>
  )
}

export default FormMetodePembayaran
