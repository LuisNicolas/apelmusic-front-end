import React from 'react'
import NavbarInvisible from '../component/NavbarInvisible'
import ContainerKonfirmasiEmailSukses from '../component/ContainerKonfirmasiEmailSukses'

const EmailConfirmSuccessPage = () => {
  return (
    <div>
      <NavbarInvisible/>
      <ContainerKonfirmasiEmailSukses/>
    </div>
  )
}

export default EmailConfirmSuccessPage
