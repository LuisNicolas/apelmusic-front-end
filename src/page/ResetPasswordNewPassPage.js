import React from 'react'
import NavbarLogin from '../component/NavbarLogin'
import ContainerPasswordNewPass from '../component/ContainerPasswordNewPass'

const ResetPasswordNewPassPage = () => {
  return (
    <div>
      <NavbarLogin/>
      <ContainerPasswordNewPass/>
    </div>
  )
}

export default ResetPasswordNewPassPage
