import { Box } from '@mui/system';
import React from 'react';
import InvoiceBreadcrumbs from '../component/Invoice/InvoiceBreadcrumbs';
import InvoiceTable from '../component/Invoice/InvoiceTable';
import NavbarLogin from '../component/NavbarLogin';
import Footer from '../component/Footer';

const InvoicePage = () => {
  return (
    <div>
      <NavbarLogin/>
      <Box sx={{maxWidth:'80%', margin:'auto', paddingBottom:20}}>
        <InvoiceBreadcrumbs/>
        <InvoiceTable/>
      </Box>
      <Footer/>
    </div>
  )
}

export default InvoicePage