import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import DetailKelasInfo from '../component/DetailKelas/DetailKelasInfo'
import Footer from '../component/Footer'
import ExploreKelas from '../component/LandingPage/ExploreKelas'
import NavbarLogin from '../component/NavbarLogin'
import { APIRequest as Axios } from '../component/Axios.js';

const DetailKelasPage = () => {
  const param = useParams();
  const id = param.id;
  const [pageData, setPageData] = useState({});

  const fetchPageData = async() =>{
    const result = await Axios.get('Class/SelectPageData?id='+id);
    setPageData(result.data);
  };
  
  useEffect(()=>{
    fetchPageData();
  },[]);

  useEffect(()=>{
    fetchPageData();
  },[id]);

  if(pageData.kategoriId!=undefined){
    return (
      <div>
        <NavbarLogin/>
        <DetailKelasInfo pageData={pageData}/>
        <ExploreKelas id={pageData.kategoriId} excludeId={id} title={"Kelas lain yang mungkin kamu suka"}/>
        <Footer/>
      </div>
    )
  }

}

export default DetailKelasPage