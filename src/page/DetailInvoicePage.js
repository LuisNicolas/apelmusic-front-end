import React, { useEffect, useState } from 'react'
import NavbarLogin from '../component/NavbarLogin';
import Footer from '../component/Footer';
import { useNavigate, useParams } from 'react-router-dom';
import DetailInvoiceBreadcrumbs from '../component/DetailInvoice/DetailInvoiceBreadcrumbs';
import DetailInvoiceDetails from '../component/DetailInvoice/DetailInvoiceDetails';
import DetailInvoiceTable from '../component/DetailInvoice/DetailInvoiceTable';
import { Box } from '@mui/system';
import jwtDecode from 'jwt-decode';

const DetailInvoicePage = () => {
  const param = useParams();
  const id = param.id;

  const navigate = useNavigate();

  const getPayloadFromToken = () => {
    const token = localStorage.getItem('token');
    try {
      const decoded = jwtDecode(token);
      return decoded;
    } catch (err) {
      return null;
    }
  };
  const token = getPayloadFromToken();
  const [email,setEmail] = useState("");

  useEffect(()=>{
    const setEmailValue = async() =>{
      if(token!==null){
          setEmail(token.email);
      }else{
        navigate("/Login");
      }
    };
    setEmailValue();
  },[]);
  
  return (
    <div>
      <NavbarLogin/>
      <Box sx={{maxWidth:'80%', margin:'auto', paddingBottom:20}}>
        <DetailInvoiceBreadcrumbs/>
        <DetailInvoiceDetails id={id}/>
        <DetailInvoiceTable id={id}/>
      </Box>
      <Footer/>
    </div>
  )
}

export default DetailInvoicePage