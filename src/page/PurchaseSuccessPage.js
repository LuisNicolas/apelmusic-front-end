import React from 'react'
import NavbarInvisible from '../component/NavbarInvisible'
import ContainerSuccessPurchase from '../component/ContainerSuccessPurchase'
const EmailConfirmSuccessPage = () => {
  return (
    <div>
      <NavbarInvisible/>
      <ContainerSuccessPurchase/>
      
    </div>
  )
}

export default EmailConfirmSuccessPage
