import React, { useEffect, useState } from 'react'
import Footer from '../component/Footer'
import ApelMusicInfo from '../component/LandingPage/ApelMusicInfo'
import BenefitInfo from '../component/LandingPage/BenefitInfo'
import ExploreKategori from '../component/LandingPage/ExploreKategori'
import ExploreKelas from '../component/LandingPage/ExploreKelas'
import NavbarLogin from '../component/NavbarLogin'
import { APIRequest as Axios } from '../component/Axios.js';

const LandingPage = () => {
  const [headerTitle, setHeaderTitle] = useState("");
  const [headerSubtitle, setHeaderSubtitle] = useState("");
  const [benefitImage, setBenefitImage] = useState("");
  const [benefitTitle, setBenefitTitle] = useState("");
  const [benefitContent, setBenefitContent] = useState("");
  const id = 0;
  const [cardData, setCardData] = useState([]);

  useEffect(()=>{
    const fetchLandingPage = async() =>{
      const result = await Axios.get('LandingPage/GetData');
      setHeaderTitle(result.data.headerTitle);
      setHeaderSubtitle(result.data.headerSubtitle);
      setBenefitImage(result.data.benefitImage);
      setBenefitTitle(result.data.benefitTitle);
      setBenefitContent(result.data.benefitContent);
    };
    fetchLandingPage();
    const fetchLPCard = async() =>{
      const result = await Axios.get('LandingPage/GetCardData');
      setCardData(result.data)
    };
    fetchLPCard();
  },[]);

  return (
    <div>
      <NavbarLogin/>
      <ApelMusicInfo headerTitle={headerTitle} headerSubtitle={headerSubtitle} cardData={cardData}/>
      <ExploreKelas id={id} excludeId={0} title={"Explore kelas favorit"}/>
      <ExploreKategori/>
      <BenefitInfo benefitImage={benefitImage} benefitTitle={benefitTitle} benefitContent={benefitContent}/>
      <Footer/>
    </div>
  )
}

export default LandingPage
