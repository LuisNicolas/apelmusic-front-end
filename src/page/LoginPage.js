import NavbarLogin from '../component/NavbarLogin';
import ContainerLogin from '../component/ContainerLogin';
import { AuthProvider } from '../Context/AuthContext';

function LoginPage() {
  return (
    <div className="App">
        <NavbarLogin />
        <ContainerLogin />
    </div>
  );
}

export default LoginPage;
