import React from 'react'
import NavBar from '../component/NavbarLogin'
import Footer from '../component/Footer'
import MyClassContainer from '../component/Kelasku/MyClassContainer'

const MyClassPage = () => {
  return (
    <div>
      <NavBar/>
      <MyClassContainer/>
      <Footer/>
    </div>
  )
}

export default MyClassPage