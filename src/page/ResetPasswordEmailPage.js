import React from 'react'
import NavbarLogin from '../component/NavbarLogin'
import ContainerPasswordEmail from '../component/ContainerPasswordEmail'

const ResetPasswordEmailPage = () => {
  return (
    <div>
      <NavbarLogin/>
      <ContainerPasswordEmail/>
    </div>
  )
}

export default ResetPasswordEmailPage
