import NavbarLogin from '../component/NavbarLogin';
import ContainerRegister from '../component/ContainerRegister';

function RegisterPage() {
  return (
    <div className="App">
      <NavbarLogin />
      <ContainerRegister />
    </div>
  );
}

export default RegisterPage;
