import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import Footer from '../component/Footer'
import ExploreKelas from '../component/LandingPage/ExploreKelas'
import CategoryInfo from '../component/ListMenuKelas/CategoryInfo'
import NavbarLogin from '../component/NavbarLogin'
import { APIRequest as Axios } from '../component/Axios.js';



const ListMenuKelasPage = () => {
  const param = useParams();
  const id = param.id;
  const [pageData, setPageData] = useState({});

  const fetchPageData = async() =>{
    const result = await Axios.get('Category/SelectPageData?id='+id);
    setPageData(result.data);
  };

  useEffect(()=>{
    fetchPageData();
  },[]);

  useEffect(()=>{
    fetchPageData();
    window.scrollTo(0,0);
  },[id]);

  return (
    <div>
      <NavbarLogin/>
      <CategoryInfo pageData={pageData} />
      <ExploreKelas id={id} excludeId={0} title={"Kelas yang tersedia"} />
      <Footer/>
    </div>
  )
}

export default ListMenuKelasPage
