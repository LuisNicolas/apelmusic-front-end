import React from 'react'
import CheckOutContent from '../component/CheckOut/CheckOutContent'
import NavbarLogin from '../component/NavbarLogin'

const CheckOutPage = () => {
  return (
    <div>
      <NavbarLogin/>
      <CheckOutContent/>
    </div>
  )
}

export default CheckOutPage