import { createTheme } from "@mui/material";

const theme = createTheme({
    typography:{
        button:{
            textTransform:'none'
        },
    },
    palette:{
        secondary:{
            main:'#5D5FEF',
        }
    },
});

export default theme;