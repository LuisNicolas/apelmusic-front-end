import * as React from 'react';
import { styled } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { Box } from '@mui/system';
import jwtDecode from 'jwt-decode';
import { useState } from 'react';
import { useEffect } from 'react';
import { APIRequest as Axios } from '../Axios.js';
import { Alert, Button, Snackbar, Typography } from '@mui/material';
import { NavLink } from 'react-router-dom';

const DetailInvoiceTable = (props) => {
    const {id} = props;
    const [listInvoiceItem, setListInvoiceItem] = useState([]);
    const accessToken = localStorage.getItem('token');
    const headers = { Authorization: `Bearer ${accessToken}` };

    useEffect(()=>{
        const fetchInvoice = async() =>{
          try{
            const result = await Axios.get('Invoice/SelectItem?id='+id,{headers});
          setListInvoiceItem(result.data);
          }catch(err){
            handleOpenSnackbar();
          }
          
        };
        fetchInvoice();
      },[]);
      const [openSnackbar, setOpenSnackbar] = useState(false);
      const handleOpenSnackbar = () =>{
        setOpenSnackbar(true);
      }
      const handleCloseSnackbar = () =>{
        setOpenSnackbar(false);
      }

    const StyledTableCell = styled(TableCell)(({ theme }) => ({
        [`&.${tableCellClasses.head}`]: {
          backgroundColor: '#F2C94C',
          color: '#4F4F4F',
        },
        [`&.${tableCellClasses.body}`]: {
          fontSize: 14,
        },
      }));
      
      const StyledTableRow = styled(TableRow)(({ theme }) => ({
        '&:nth-of-type(even)': {
          backgroundColor: '#F2C94C33',
        },
        '&:last-child td, &:last-child th': {
          border: 0,
        },
      }));

  return (
    <div>
        <TableContainer component={Paper}>
        <Table sx={{ minWidth: 700 }} aria-label="customized table">
            <TableHead>
            <TableRow>
                <StyledTableCell align ="center">No</StyledTableCell>
                <StyledTableCell align="center">Nama Course</StyledTableCell>
                <StyledTableCell align="center">Kategori</StyledTableCell>
                <StyledTableCell align="center">Jadwal</StyledTableCell>
                <StyledTableCell align="center">Harga</StyledTableCell>
            </TableRow>
            </TableHead>
            <TableBody>
            {listInvoiceItem.map((row,index) => (
                <StyledTableRow key={index}>
                    <StyledTableCell component="th" scope="row" align="center">
                        {index+1}
                    </StyledTableCell>
                    <StyledTableCell align="center">{row.name}</StyledTableCell>
                    <StyledTableCell align="center">{row.category}</StyledTableCell>
                    <StyledTableCell align="center">{new Date(row.jadwal).toLocaleDateString("id-ID",{weekday:"long", year: "numeric",month: "long",day: "numeric"})}</StyledTableCell>
                    <StyledTableCell align="center">IDR {row.harga.toLocaleString("de-DE")}</StyledTableCell>
                </StyledTableRow>
            ))}
            </TableBody>
        </Table>
        </TableContainer>
        <Snackbar open={openSnackbar} autoHideDuration={6000} onClose={handleCloseSnackbar}>
          <Alert onClose={handleCloseSnackbar} severity="error" sx={{ width: '100%' }}>
              Unauthorize Access!
          </Alert>
        </Snackbar>
    </div>
  )
}

export default DetailInvoiceTable
