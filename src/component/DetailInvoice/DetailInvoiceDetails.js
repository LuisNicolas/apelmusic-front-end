import { Alert, Grid, Snackbar, Typography } from '@mui/material';
import React, { useEffect, useState } from 'react'
import { APIRequest as Axios } from '../Axios.js';

const DetailInvoiceDetails = (props) => {
    const {id} = props;
    const [invoiceDetail, setInvoiceDetail] = useState({});
    const accessToken = localStorage.getItem('token');
    const headers = { Authorization: `Bearer ${accessToken}` };

    useEffect(()=>{
        const fetchInvoice = async() =>{
          try{
            const result = await Axios.get('Invoice/SelectDetail?id='+id,{headers});
            setInvoiceDetail(result.data);
          }catch(err){
            handleOpenSnackbar();
          }
        };
        fetchInvoice();
      },[]);
      const [openSnackbar, setOpenSnackbar] = useState(false);
      const handleOpenSnackbar = () =>{
        setOpenSnackbar(true);
      }
      const handleCloseSnackbar = () =>{
        setOpenSnackbar(false);
      }

    if(invoiceDetail.totalHarga!= undefined){
        return (
            <div style={{marginBottom:20}}>
                <Typography variant='h5' sx={{textAlign:'left',marginBottom:3}}>
                    Rincian Invoice
                </Typography>
                <Grid container spacing={1}>
                    <Grid item xs={2} sx={{textAlign:'left'}}>
                        <Typography variant='h7'>
                            No. Invoice : 
                        </Typography>
                    </Grid>
                    <Grid item xs={10} sx={{textAlign:'left'}}>
                        <Typography variant='h7'>
                            APM{('0000'+invoiceDetail.id).slice(-4)}
                        </Typography>
                    </Grid>
                    <Grid item xs={2} sx={{textAlign:'left'}}>
                        <Typography variant='h7'>
                            Tanggal Beli : 
                        </Typography>
                    </Grid>
                    <Grid item xs={4} sx={{textAlign:'left'}}>
                        <Typography variant='h7'>
                            {new Date(invoiceDetail.tglBeli).toLocaleDateString("id-ID",{year: "numeric",month: "long",day: "numeric"})}
                        </Typography>
                    </Grid>
                    <Grid item xs={4} sx={{textAlign:'right'}}>
                        <Typography variant='h7'>
                            Total Harga :
                        </Typography>
                    </Grid>
                    <Grid item xs={2} sx={{textAlign:'right'}}>
                        <Typography variant='h7'>
                            IDR {invoiceDetail.totalHarga.toLocaleString("de-DE")}
                        </Typography>
                    </Grid>
                </Grid>
                <Snackbar open={openSnackbar} autoHideDuration={6000} onClose={handleCloseSnackbar}>
                    <Alert onClose={handleCloseSnackbar} severity="error" sx={{ width: '100%' }}>
                        Unauthorize Access!
                    </Alert>
                </Snackbar>
            </div>
          )
    }
}

export default DetailInvoiceDetails
