import { Breadcrumbs, Typography } from '@mui/material'
import React from 'react'
import { NavLink } from 'react-router-dom'

const DetailInvoiceBreadcrumbs = () => {
  return (
    <div style={{margin:'30px 0px 30px 0px'}}>
        <Breadcrumbs separator="›" aria-label="breadcrumb">
            <NavLink to='/' color="gray" style={{textDecoration:'none',color:'gray'}}>
                Beranda
            </NavLink>,
            <NavLink to='/Invoice' color="gray" style={{textDecoration:'none',color:'gray'}}>
                Invoice
            </NavLink>,
            <Typography color="#5D5FEF">
                Rincian Invoice
            </Typography>
        </Breadcrumbs>
    </div>
  )
}

export default DetailInvoiceBreadcrumbs
