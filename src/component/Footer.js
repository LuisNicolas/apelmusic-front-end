import styled from '@emotion/styled';
import { AppBar, Box, Button, SvgIcon, Toolbar, Typography, useStepContext } from '@mui/material';
import React, { useEffect, useState } from 'react';
import LocalPhoneRoundedIcon from '@mui/icons-material/LocalPhoneRounded';
import InstagramIcon from '@mui/icons-material/Instagram';
import YouTubeIcon from '@mui/icons-material/YouTube';
import SendIcon from '@mui/icons-material/Send';
import EmailIcon from '@mui/icons-material/Email';
import { NavLink } from 'react-router-dom';
import { APIRequest as Axios } from './Axios.js';

const SectionTitle = styled(Typography)(({theme})=>({
    fontWeight: 650,
    color: 'black',
    textDecoration: 'none',
    marginRight: "auto",
    marginTop: '20px',
    textAlign: 'left'
}));
const SectionContent = styled(Typography)(({theme})=>({
    color: 'black',
    textDecoration: 'none',
    marginRight: "auto",
    marginTop: '5px',
    textAlign:'justify'
}));

const ContactIcon = styled(Button)(({theme})=>({
    color:'#F2C94C',
    backgroundColor:'#5D5FEF',
    borderRadius:'100%',
    height:'60px',
    marginLeft:'5px'
}));

const NavLink2 = styled(NavLink)(({theme})=>({
    textDecoration:'none'
}));

const Footer = () => {
    const [categoryList,setCategoryList] = useState([]);

    const [aboutTitle, setAboutTitle] = useState("");
    const [aboutContent, setAboutContent] = useState("");
    const [productTitle, setProductTitle] = useState("");
    const [addressTitle, setAddressTitle] = useState("");
    const [addressContent, setAddressContent] = useState("");
    const [contactTitle, setContactTitle] = useState("");

    useEffect(()=>{
      const fetchFooter = async() =>{
        const result = await Axios.get('Footer/Content');
        setAboutTitle(result.data.aboutTitle);
        setAboutContent(result.data.aboutContent);
        setProductTitle(result.data.productTitle);
        setAddressTitle(result.data.addressTitle);
        setAddressContent(result.data.addressContent);
        setContactTitle(result.data.contactTitle);
      };
      fetchFooter();
      const fetchFooterCategory = async() =>{
        const result = await Axios.get('Footer/Category');
        setCategoryList(result.data);
      }
      fetchFooterCategory();
    },[]);


  return (
    <div>
    <Box sx={{ position:'relative',bottom:0,left:0,right:0}}>
      <AppBar position="static">
        <Toolbar sx={{ background: "#F2C94C", flex:'1 0 auto',alignItems:'stretch', display: 'flex',gap:'30px',paddingBottom:'20px'}}>
          <Box sx={{height:'100%',width:'100%',}}>
          <SectionTitle sx={{gridRow:1}}>{aboutTitle}</SectionTitle>
          <SectionContent sx={{gridRow:'2/ span 2'}}>{aboutContent}</SectionContent>
          </Box>
          <Box sx={{height:'100%',width:'100%'}}>
          <SectionTitle sx={{gridRow:1}}>{productTitle}</SectionTitle>
            <ul style={{display:'flex',flexWrap:'wrap',flexDirection:'row',gap:'10px 160px',gridColumn:2,gridRow:'2/ span 2',color:'black',marginTop:5}}>
                {categoryList.map((item,index)=>(
                    <li key={index} style={{minWidth:'40px'}}>
                      <NavLink2 to={{pathname:"/ListMenuKelas/"+item.id}}>
                        <SectionContent>{item.nama}</SectionContent>
                      </NavLink2>
                    </li>
                ))}
            </ul>
          </Box>
          <Box sx={{height:'100%',width:'100%'}}>
          <SectionTitle sx={{gridRow:1}}>{addressTitle}</SectionTitle>
          <SectionContent sx={{gridRow:2}}>{addressContent}</SectionContent>
          <SectionTitle sx={{gridRow:3}}>{contactTitle}</SectionTitle>
          <SectionContent sx={{gridRow:4}}>
          <a href="https://coding.id" style={{textDecoration:'none'}}>
            <ContactIcon variant="contained">
                <LocalPhoneRoundedIcon/>
            </ContactIcon>
            </a>
            <a href="https://instagram.com" style={{textDecoration:'none'}}>
            <ContactIcon variant="contained">
                <InstagramIcon/>
            </ContactIcon>
            </a>
            <a href="https://youtube.com" style={{textDecoration:'none'}}>
            <ContactIcon variant="contained">
                <YouTubeIcon/>
            </ContactIcon>
            </a>
            <a href="https://coding.id" style={{textDecoration:'none'}}>
            <ContactIcon variant="contained">
                <SendIcon/>
            </ContactIcon>
            </a>
            <a href="https://coding.id" style={{textDecoration:'none'}}>
            <ContactIcon variant="contained">
                <EmailIcon/>
            </ContactIcon>
            </a>
          </SectionContent>
          </Box>
        </Toolbar>
      </AppBar>
    </Box>
    </div>
  )
}

export default Footer
