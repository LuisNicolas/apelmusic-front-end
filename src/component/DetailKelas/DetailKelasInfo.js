import { Alert, Button, FormControl, Grid, InputLabel, MenuItem, Select, Snackbar, Typography } from '@mui/material'
import React, { useEffect, useState } from 'react'
import moment from 'moment';
import { Box } from '@mui/system';
import { APIRequest as Axios } from '../Axios.js';
import { NavLink, useNavigate } from 'react-router-dom';
import jwtDecode from 'jwt-decode';

const DetailKelasInfo = (props) => {
    const {pageData} = props;
    const navigate = useNavigate();

    const getPayloadFromToken = () => {
        const token = localStorage.getItem('token');
        try {
          const decoded = jwtDecode(token);
          return decoded;
        } catch (err) {
          return null;
        }
      };
    const token = getPayloadFromToken();
    const accessToken = localStorage.getItem('token');
    const [email,setEmail] = useState("");

    const [dateList, setDateList] = useState([]);

    const current = new Date();
    const dateNumber =current.getDate();
    const month = current.getMonth();
    const year = current.getFullYear(); 

    useEffect(()=>{
        const getDateList = async() =>{
            for(let x =1;x<=7;x++){
                const date = moment(new Date(year,month,dateNumber+x)).format("DD MMMM YYYY");
                setDateList(currentDateList => [...currentDateList, date])
            }
        };
        getDateList();
        const setEmailValue = async() =>{
            if(token!==null){
                setEmail(token.email);
            }
        };
        setEmailValue();
      },[]);
    useEffect(()=>{
     window.scrollTo(0,0);
    },[props])

      const [buttonDisable,setButtonDisable] = useState(true);
      const [dateValue, setDateValue] = useState("");
      const [realDateValue, setRealDateValue] = useState();

      const handleChange = (event) => {
        setDateValue(event.target.value);
        setButtonDisable(false);
        dateList.map((item,index) =>{
            if(item===event.target.value){
                setRealDateValue(item);
            }
          })
      };

      const saveToDb = async () =>{
        if(email!==""){
            let formData = new FormData();
            formData.append("Id",0);
            formData.append("KelasId", pageData.id);
            formData.append("Jadwal",realDateValue);
            formData.append("Email",email);
            Axios({
              method:"POST",
              url:"Basket/Insert",
              data: formData,
              headers:{
                "Content-Type":"multipart/form-data",
                "Authorization": `Bearer ${accessToken}`
              }
            })
            .then((res)=>{
              if(res.status === 200){
                openSnackBar();
              }
            })
            .catch((err) =>{
              handleOpenSnackbar();
            })
        }else{
            navigate("/Login");
        };
    }
    
    const handleBeliSekarang = () =>{
        if(email!==""){
                let formData = new FormData();
                formData.append("Id",0);
                formData.append("KelasId", pageData.id);
                formData.append("Jadwal",realDateValue);
                formData.append("Email",email);
                Axios({
                  method:"POST",
                  url:"Basket/Insert",
                  data: formData,
                  headers:{
                    "Content-Type":"multipart/form-data",
                    "Authorization": `Bearer ${accessToken}`
                  }
                })
                .then((res)=>{
                  if(res.status === 200){
                    navigate("/CheckOut/");
                  }
                })
                .catch((err) =>{
                  handleOpenSnackbar();
                })
        }else{
            navigate("/Login");
        }
    }

    const [open, setOpen] = React.useState(false);

    const openSnackBar = () => {
        setOpen(true);
    };

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
        return;
        }
        setOpen(false);
    };
    const [openSnackbar, setOpenSnackbar] = useState(false);
    const handleOpenSnackbar = () =>{
      setOpenSnackbar(true);
    }
    const handleCloseSnackbar = () =>{
      setOpenSnackbar(false);
    }

    if(pageData.gambar!=null){
        return (
            <div>
              <Grid container sx={{justifyContent:'center',marginTop:5,gap:5,marginBottom:20}}>
                <Grid item xs={3}>
                    <img src={pageData.gambar} alt="Image not Found" style={{maxHeight:'100%',maxWidth:'100%'}}/>
                </Grid>
                <Grid item xs={7} sx={{textAlign:'left'}}>
                    <Typography variant="h7" sx={{color:'gray'}}>
                        {pageData.kategoriName}
                    </Typography>
                    <Typography variant="h5">
                        {pageData.nama}
                    </Typography>
                    <Typography variant="h6" sx={{color:'#5D5FEF',marginTop:2,marginBottom:2}}>
                        IDR {pageData.harga.toLocaleString("de-DE")}
                    </Typography>
                    <Grid item xs={4}>
                        <FormControl fullWidth>
                        <InputLabel id="date-select-label">Pilih jadwal kelas</InputLabel>
                        <Select
                        labelId='date-select-label'
                        id="date-select"
                        label="Date"
                        value={dateValue}
                        onChange={handleChange}
                        >
                            {dateList.map((item,index) => (
                                <MenuItem key={index} value={item}>{new Date(item).toLocaleDateString("id-ID",{weekday: "long",year: "numeric",month: "long",day: "numeric"})}</MenuItem>
                            ))}
                        </Select>
                        </FormControl>
                    </Grid>
                    <Box sx={{paddingTop:5}}>
                        <Button sx={{width:250,backgroundColor:'white',color:'#5D5FEF',border:'1px solid #5D5FEF',borderRadius:'8px',padding:'10px 20px 10px 20px', marginRight:5}} onClick={saveToDb} disabled={buttonDisable}>Masukkan Keranjang</Button>
                        <Button variant="contained" sx={{width:250,backgroundColor:'#5D5FEF',color:'white',borderRadius:'8px',padding:'10px 20px 10px 20px'}} disabled={buttonDisable} onClick={handleBeliSekarang}>
                                Beli sekarang
                         </Button>
                    </Box>
                </Grid>
                <Grid item xs={10} sx={{textAlign:'left'}}>
                    <Typography variant="h5">
                        Deskripsi
                    </Typography>
                    <Typography variant="h7" sx={{textAlign:'justify'}}>
                        {pageData.deskripsi}
                    </Typography>
                </Grid>
              </Grid>
              <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
                    <Alert onClose={handleClose} severity="success" sx={{ width: '100%' }}>
                        Kelas berhasil ditambahkan ke dalam keranjang!
                    </Alert>
                </Snackbar>
                <Snackbar open={openSnackbar} autoHideDuration={6000} onClose={handleCloseSnackbar}>
                  <Alert onClose={handleCloseSnackbar} severity="error" sx={{ width: '100%' }}>
                      Unauthorize Access!
                  </Alert>
                </Snackbar>
            </div>
          )
    }
}

export default DetailKelasInfo
