import styled from '@emotion/styled';
import { Card, Grid, Skeleton, Typography } from '@mui/material';
import React, { useEffect, useState } from 'react';
import { NavLink } from 'react-router-dom';
import { APIRequest as Axios } from '../Axios.js';

const ExploreKelas = (props) => {
    const {id,excludeId,title} = props;
    const [judul, setJudul] = useState(title);
    const [listKelas, setListKelas] = useState([]);

    useEffect(()=>{
        fetchClass();
      },[]);
  
    useEffect(()=>{
      fetchClass();
    },[props]);
  
    const fetchClass = async() =>{
      const result = await Axios.get('Class/SelectTop?id='+id+'&excludeId='+excludeId);
      setListKelas(result.data);
    };

    const SectionTitle = styled(Typography)(({theme})=>({
        fontWeight: 650,
        fontSize:24,
        color: '#5D5FEF',
        textDecoration: 'none',
        margin:60,
        paddingTop: '20px', paddingBottom: '2px'
      }));

      const CourseTitle = styled(Typography)(({theme})=>({
        fontWeight: 650,
        fontSize:20,
        color: 'black',paddingBottom: '20px'
      }));

      const CourseCategory = styled(Typography)(({theme})=>({
        fontWeight: 650,
        fontSize:12,
        color: 'gray',
        paddingTop: '20px', paddingBottom: '2px'
      }));

      const CoursePrice = styled(Typography)(({theme})=>({
        fontWeight: 650,
        fontSize:18,
        color: '#5D5FEF',
        paddingTop: '20px', paddingBottom: '2px'
      }));

      if(listKelas.length!=0){
        return (
          <div style={{paddingBottom:100, width:'90%',margin:'auto'}}>
            <SectionTitle>{judul}</SectionTitle>
              <Grid container sx={{display:'flex',flexWrap:'wrap',flexDirection:'row',gap:'0px 0px', width:'100%',color:'black', padding:0,listStyleType:'none',justifyContent:'left'}}>
                      {listKelas.map((item,index)=>(
                          <Grid item xs={4} key={index} style={{width:'auto',maxWidth:'350px',textAlign:'left',margin:'30px 52px 30px 52px'}}>
                              <NavLink style={{textDecoration:'none'}} to={{pathname:"/DetailKelas/"+item.id}}>
                                <Card sx={{boxShadow:'none'}}>
                                  <img src={item.gambar} alt="Image not Found" style={{maxHeight:'100%',maxWidth:'100%'}}/>
                                  <CourseCategory>{item.kategoriName}</CourseCategory>
                                  <CourseTitle>{item.nama}</CourseTitle>
                                  <CoursePrice>IDR {item.harga.toLocaleString("de-DE")}</CoursePrice>
                                </Card>
                              </NavLink>
                          </Grid>
                      ))}
                  </Grid>
          </div>
        )
      }else{
        return (
          <div style={{paddingBottom:100, width:'90%',margin:'auto'}}>
            <SectionTitle>{judul}</SectionTitle>
              <Grid container sx={{display:'flex',flexWrap:'wrap',flexDirection:'row',gap:'0px 0px', width:'100%',color:'black', padding:0,listStyleType:'none',justifyContent:'left'}}>
                          <Grid item xs={4}style={{width:'auto',maxWidth:'350px',textAlign:'left',margin:'30px 52px 30px 52px'}}>
                                <Card sx={{boxShadow:'none'}}>
                                  <Skeleton variant='rounded' height={234} width={350} />
                                  <CourseCategory><Skeleton variant='rectangular' height={20} width={350}/></CourseCategory>
                                  <CourseTitle><Skeleton variant='rectangular' height={80} width={350}/></CourseTitle>
                                  <CoursePrice><Skeleton variant='rectangular' height={30} width={350}/></CoursePrice>
                                </Card>
                          </Grid>
                          <Grid item xs={4}style={{width:'auto',maxWidth:'350px',textAlign:'left',margin:'30px 52px 30px 52px'}}>
                                <Card sx={{boxShadow:'none'}}>
                                  <Skeleton variant='rounded' height={234} width={350} />
                                  <CourseCategory><Skeleton variant='rectangular' height={20} width={350}/></CourseCategory>
                                  <CourseTitle><Skeleton variant='rectangular' height={80} width={350}/></CourseTitle>
                                  <CoursePrice><Skeleton variant='rectangular' height={30} width={350}/></CoursePrice>
                                </Card>
                          </Grid>
                          <Grid item xs={4}style={{width:'auto',maxWidth:'350px',textAlign:'left',margin:'30px 52px 30px 52px'}}>
                                <Card sx={{boxShadow:'none'}}>
                                  <Skeleton variant='rounded' height={234} width={350} />
                                  <CourseCategory><Skeleton variant='rectangular' height={20} width={350}/></CourseCategory>
                                  <CourseTitle><Skeleton variant='rectangular' height={80} width={350}/></CourseTitle>
                                  <CoursePrice><Skeleton variant='rectangular' height={30} width={350}/></CoursePrice>
                                </Card>
                          </Grid>
                </Grid>
          </div>
        )
      }
  
}

export default ExploreKelas
