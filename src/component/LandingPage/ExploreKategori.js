import styled from '@emotion/styled';
import { Skeleton, Typography } from '@mui/material';
import React, { useEffect, useState } from 'react'
import { NavLink } from 'react-router-dom';
import { APIRequest as Axios } from '../Axios.js';

const ExploreKategori = () => {
    const [judul, setJudul] = useState("Pilih kelas impian kamu");
    const [listKategori, setListKategori] = useState([]);

    useEffect(()=>{
        const fetchCategory = async() =>{
          const result = await Axios.get('Category/');
          setListKategori(result.data);
        };
        fetchCategory();
      },[]);
  
    const SectionTitle = styled(Typography)(({theme})=>({
        fontWeight: 650,
        fontSize:24,
        color: '#5D5FEF',
        textDecoration: 'none',
        margin:60,
        paddingTop: '20px', paddingBottom: '2px'
      }));

      const CourseTitle = styled(Typography)(({theme})=>({
        fontSize:22,
        color: 'black',
        padding: '20px'
      }));

  if(listKategori.length !=0){
    return (
      <div style={{paddingBottom:100, backgroundColor:'#F6F6FE'}}>
        <SectionTitle>{judul}</SectionTitle>
        <ul style={{display:'flex',flexWrap:'wrap',flexDirection:'row',gap:'30px 30px', width:'100%',color:'black', padding:0,listStyleType:'none',justifyContent:'center'}}>
                  {listKategori.map((item,index)=>(
                      <li key={index} style={{width:'auto',minWidth:150,paddingLeft:70,paddingRight:70,textAlign:'center'}}>
                          <NavLink style={{textDecoration:'none'}} to={{pathname:"/ListMenuKelas/"+item.id}}>
                              <img src={item.gambar} alt="Image not Found" style={{maxHeight:'100%',maxWidth:'100%'}}/>
                              <CourseTitle>{item.nama}</CourseTitle>
                          </NavLink>
                          
                      </li>
                  ))}
              </ul>
      </div>
    )
  }else{
    return(
      <div style={{paddingBottom:100, backgroundColor:'#F6F6FE'}}>
        <SectionTitle>{judul}</SectionTitle>
        <ul style={{display:'flex',flexWrap:'wrap',flexDirection:'row',gap:'30px 30px', width:'100%',color:'black', padding:0,listStyleType:'none',justifyContent:'center'}}>
          <li style={{width:'auto',minWidth:150,paddingLeft:70,paddingRight:70}}>
            <Skeleton variant='rounded' height={144} width={130}/>
          </li>
          <li style={{width:'auto',minWidth:150,paddingLeft:70,paddingRight:70}}>
            <Skeleton variant='rounded' height={144} width={130}/>
          </li>
          <li style={{width:'auto',minWidth:150,paddingLeft:70,paddingRight:70}}>
            <Skeleton variant='rounded' height={144} width={130}/>
          </li>
          <li style={{width:'auto',minWidth:150,paddingLeft:70,paddingRight:70}}>
            <Skeleton variant='rounded' height={144} width={130}/>
          </li>
          <li style={{width:'auto',minWidth:150,paddingLeft:70,paddingRight:70}}>
            <Skeleton variant='rounded' height={144} width={130}/>
          </li>
          <li style={{width:'auto',minWidth:150,paddingLeft:70,paddingRight:70}}>
            <Skeleton variant='rounded' height={144} width={130}/>
          </li>
          <li style={{width:'auto',minWidth:150,paddingLeft:70,paddingRight:70}}>
            <Skeleton variant='rounded' height={144} width={130}/>
          </li>
          <li style={{width:'auto',minWidth:150,paddingLeft:70,paddingRight:70}}>
            <Skeleton variant='rounded' height={144} width={130}/>
          </li>
        </ul>
      </div>
    )
  }

  
}

export default ExploreKategori
