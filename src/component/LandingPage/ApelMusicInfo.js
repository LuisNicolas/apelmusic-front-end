import styled from '@emotion/styled'
import { Card, CardActionArea, CardContent, CardMedia, Skeleton, Typography } from '@mui/material'
import { Box } from '@mui/system'
import React, { useState } from 'react'
import './ApelMusicInfo.css'

const ApelMusicInfo = (props) => {
  const {headerTitle, headerSubtitle, cardData} = props;

  const SectionTitle = styled(Typography)(({theme})=>({
    mr: 1,
    fontWeight: 650,
    fontSize:35,
    color: 'white',
    textDecoration: 'none',
    marginRight: "auto",
    paddingTop: '20px', paddingBottom: '2px'
  }));

  const SectionSubTitle = styled(Typography)(({theme})=>({
    fontSize:28,
    color: 'white',
    textDecoration: 'none',
    marginRight: "auto",
    paddingTop: '20px', paddingBottom: '20px'
  }));

  const CardTitle = styled(Typography)(({theme})=>({
    fontWeight: 550,
    fontSize:50,
    color: '#5D5FEF',
    textDecoration: 'none',
    marginRight: "auto",
    paddingBottom: '20px'
  }));

  const CardDescription = styled(Typography)(({theme})=>({
    fontSize:17,
    fontWeight:550,
    color: 'black',
    textDecoration: 'none',
    marginRight: "auto",
    paddingTop: '20px', 
    paddingBottom: '45px'
  }));

  const CardStyled = styled(Card)(({theme})=>({
    margin:'20px',
    borderRadius:'15px'
  }));

  return (
    <div className='background'>
      <Box sx={{textAlign:'center'}}>
        { headerTitle ? (
          <SectionTitle>{headerTitle}</SectionTitle>
        ):(
          <SectionTitle><Skeleton variant="rectangular"/></SectionTitle>
        )}
        { headerSubtitle ? (
        <SectionSubTitle>{headerSubtitle}</SectionSubTitle>
        ):(
          <SectionTitle><Skeleton variant="rectangular"/></SectionTitle>
        )}
        {
          cardData.length ? (
          <ul style={{display:'flex',flexWrap:'wrap',flexDirection:'row',gap:'30px 30px', width:'100%',color:'black', padding:0,listStyleType:'none',gridTemplateColumns:'minmax(35%,1fr) 40%',justifyContent:'center'}}>
          {cardData.map((item,index) => (
            <li key={index}>
                <CardStyled sx={{maxWidth:345}}>
                <CardContent>
                <CardTitle>{item.cardHeader}</CardTitle>
                <CardDescription>{item.cardContent}</CardDescription>
                </CardContent>
              </CardStyled>
            </li>
          ))};
        </ul>
          ):(
          <ul style={{display:'flex',flexWrap:'wrap',flexDirection:'row',gap:'30px 30px', width:'100%',color:'black', padding:0,listStyleType:'none',gridTemplateColumns:'minmax(35%,1fr) 40%',justifyContent:'center'}}>
            <li>
              <Skeleton variant='rounded' width={385} height={291}/>
            </li>
            <li>
              <Skeleton variant='rounded' width={385} height={291} />
            </li>
            <li>
              <Skeleton variant='rounded' width={385} height={291} />
            </li>
          </ul>
          )
        }
        
      </Box>
    </div>
  )
}

export default ApelMusicInfo
