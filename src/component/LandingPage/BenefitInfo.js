import styled from '@emotion/styled';
import { Skeleton, Typography } from '@mui/material';
import React, { useState } from 'react'

const BenefitInfo = (props) => {
    const {benefitImage, benefitTitle, benefitContent} = props;

    const SectionTitle = styled(Typography)(({theme})=>({
        fontWeight: 650,
        fontSize:24,
        color: '#5D5FEF',
        textDecoration: 'none',
        paddingTop: '20px',
        textAlign:'left'
      }));
      const SectionDescription = styled(Typography)(({theme})=>({
        fontSize:17,
        color: 'black',
        textDecoration: 'none',
        marginRight: "auto",
        paddingTop: '20px', 
        paddingBottom: '45px',
        textAlign:'justify'
      }));
  return (
    <div style={{display:'flex',flexFlow:'row',flexWrap:'wrap',paddingTop:100,paddingBottom:150,justifyContent:'center'}}>
        {benefitImage ? (
          <img src={benefitImage} style={{maxHeight:450,maxHeight:450}}/>
        ):(
          <Skeleton variant='rectangular' height={450} width={450} />
        )}
        
        <div style={{display:'flex',flexFlow:'column',maxWidth:'60%',marginTop:55}}>        
            {benefitTitle ? (
              <SectionTitle>{benefitTitle}</SectionTitle>
            ):(
              <SectionTitle><Skeleton variant='rectangular' width={912} height={56}/></SectionTitle>
            )}
            {benefitContent ? (
              <SectionDescription>{benefitContent}</SectionDescription>
            ):(
              <SectionDescription><Skeleton variant='rectangular' width={912} height={300}/></SectionDescription>
            )}
            
        </div>
    </div>
  )
}

export default BenefitInfo
