import * as React from 'react';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { APIRequest as Axios } from '../component/Axios.js';
import { Snackbar } from '@mui/material';
import MuiAlert from '@mui/material/Alert';
import { NavLink } from 'react-router-dom';

const theme = createTheme();

export default function Register() {
    //useEffectPasswordConfirmation
    const [passwordError, setPasswordError] = React.useState(false);
    const [passwordLengthError, setPasswordLengthError] = React.useState(false);
    const [passwordUppercaseError, setPasswordUppercaseError] = React.useState(false);
    const [passwordSymbolError, setPasswordSymbolError] = React.useState(false);

    const [open, setOpen] = React.useState(false);
    const [severity, setSeverity] = React.useState('success');
    const [message, setMessage] = React.useState('');

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpen(false);
    };

    const handleSubmit = (event) => {
        event.preventDefault();
        const data = new FormData(event.currentTarget);
        const password = data.get('password');

        if (password.length < 8) {
            setPasswordLengthError(true);
            return;
        } else {
            setPasswordLengthError(false);
        }

        if (!/[A-Z]/.test(password)) {
            setPasswordUppercaseError(true);
            return;
        } else {
            setPasswordUppercaseError(false);
        }

        if (!/[!@#$%^&*()_+\-=[\]{};':"\\|,.<>/?]/.test(password)) {
            setPasswordSymbolError(true);
            return;
        } else {
            setPasswordSymbolError(false);
        }

        if (data.get('password') !== data.get('passwordConfirm')) {
            setPasswordError(true);
            return;
        } else {
            Axios({
                method: "POST",
                url: "Register/Insert",
                data: {
                    username: data.get('name'),
                    email: data.get('email'),
                    password: data.get('password')
                },
                headers: {
                    "Content-Type": "application/json"
                }
            })
                .then((res) => {
                    if (res.status === 200) {
                        setSeverity('success');
                        setMessage('Register success please check your email to activate your account');
                    }
                })
                .catch((err) => {
                    setSeverity('error');
                    setMessage('Register failed email already exist');
                })
            //refresh();
            console.log({
                name: data.get('name'),
                email: data.get('email'),
                password: data.get('password'),
                passwordConfirm: data.get('passwordConfirm'),
            });
            setPasswordError(false);
            setOpen(true);

        }
    };

    return (
        <ThemeProvider theme={theme}>
            <Container component="main" maxWidth="md">
                <CssBaseline />
                <Box
                    sx={{
                        marginTop: 20,
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                    }}
                >

                    <Grid container justify="flex-start">
                        <Grid>
                            <Typography component="h1" variant="h1" align='left' sx={{ fontSize: '24px' }}>
                                Selamat Datang Musikers!
                            </Typography>
                            <Grid>
                                <Typography component="h2" variant="h2" align='left' sx={{ fontSize: '16px' }}>
                                    Yuk Daftar terlebih dahulu akun kamu
                                </Typography>
                            </Grid>
                        </Grid>

                    </Grid>
                    <Box component="form" onSubmit={handleSubmit} sx={{ mt: 1 }}>
                        <TextField
                            margin="normal"
                            required
                            fullWidth
                            id="name"
                            label="Masukkan Nama Lengkap"
                            name="name"
                            autoComplete="name"
                            autoFocus
                        />
                        <TextField
                            margin="normal"
                            required
                            fullWidth
                            name="email"
                            label="Masukkan Email"
                            type="email"
                            id="email"
                            autoComplete="email"
                        />
                        <TextField
                            margin="normal"
                            required
                            fullWidth
                            name="password"
                            label="Masukkan Password"
                            type="password"
                            id="password"
                            error={passwordLengthError || passwordUppercaseError || passwordSymbolError}
                            helperText={
                                passwordLengthError
                                    ? "Password harus memiliki panjang minimal 8 karakter"
                                    : passwordUppercaseError
                                        ? "Password harus memiliki setidaknya 1 huruf besar"
                                        : passwordSymbolError
                                            ? "Password harus memiliki setidaknya 1 simbol (!@#$%^&*()_+-=[]{};':\"\\|,.<>/?)"
                                            : ""

                            }
                        />
                        <TextField
                            margin="normal"
                            required
                            fullWidth
                            name="passwordConfirm"
                            label="Konfirmasi Password"
                            type="password"
                            id="passwordConfirm"
                            error={passwordError}
                            helperText={passwordError ? "Password dan Konfirmasi Password tidak sama, mohon cek kembali" : ""}
                        />
                        <Grid container justify="center">
                            <Grid item>
                                <Button
                                    type="submit"
                                    variant="contained"
                                    sx={{
                                        width: '150px', mt: 3, mb: 2, background: '#5D5FEF', borderRadius: '8px', textTransform: 'none'
                                    }}
                                >
                                    Daftar
                                </Button>
                            </Grid>
                            <Grid item>
                                <Typography sx={{ alignSelf: 'center', justifySelf: 'end', marginTop: '30px', marginLeft: '30px' }}>
                                    Sudah punya akun?
                                    <NavLink to='/Login' sx={{ textDecoration: 'none' }}>Login disini</NavLink>
                                </Typography>
                            </Grid>
                        </Grid>
                    </Box>
                </Box>
            </Container>
            <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
                <MuiAlert elevation={6} variant="filled" onClose={handleClose} severity={severity}>
                    {message}
                </MuiAlert>
            </Snackbar>
        </ThemeProvider>
    );
}