import React, { useState } from 'react'
import { Box, Container } from '@mui/system'
import { colors, Typography } from '@mui/material'
import { NavLink, useNavigate } from 'react-router-dom'
import { Grid } from '@mui/material'
import Button from '@mui/material/Button'
import EmailPurchase from '../Images/EmailPurchase.PNG'
import { useParams } from 'react-router-dom'
import { APIRequest as Axios } from '../component/Axios.js';
import { useEffect } from 'react'
import HomeIcon from '@mui/icons-material/Home';
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import jwtDecode from 'jwt-decode'


function ContainerSuccessPurchase() {
    const navigate = useNavigate();

    const getPayloadFromToken = () => {
      const token = localStorage.getItem('token');
      try {
        const decoded = jwtDecode(token);
        return decoded;
      } catch (err) {
        return null;
      }
    };
    const token = getPayloadFromToken();
    const [email,setEmail] = useState("");

    useEffect(()=>{
        const setEmailValue = async() =>{
          if(token!==null){
              setEmail(token.email);
          }else{
            navigate("/Login");
          }
        };
        setEmailValue();
      },[]);

    return (
        <div>
            <Container maxWidth="s" sx={{ marginTop: '122px' }}>
                <Box>
                    <img src={EmailPurchase} style={{ width: '250px', height: '250px' }} />
                </Box>
                <Typography variant="h6" sx={{ fontSize: '24px', fontWeight: 700, marginTop: '20px', color: '#5D5FEF' }}>
                    Pembelian Berhasil
                </Typography>
                <Typography variant="h6" sx={{ fontSize: '16px', fontWeight: 400, marginTop: '20px' }}>
                    Yey! Kamu telah berhasil membeli kursus di Apel Music
                </Typography>
                <Grid container justify="center">
                    <Grid item xs={4}>

                    </Grid>
                    <Grid item xs={2}>
                        <NavLink to='/' style={{ textDecoration: 'none' }}>
                            <Button
                                sx={{
                                    width: '150px',
                                    mt: 3,
                                    mb: 2,
                                    background: 'white',
                                    border: '2px solid #5D5FEF',
                                    borderRadius: '8px',
                                    color: '#5D5FEF',
                                    textTransform: 'none'
                                }}
                            >
                                <Grid container justify="center">
                                    <Grid item xs={2}>
                                        <HomeIcon sx={{ color: '#5D5FEF' }} />
                                    </Grid>
                                    <Grid item xs={10}>
                                        <Typography sx={{ fontFamily: 'Poppins' }}>
                                            Ke Beranda
                                        </Typography>
                                    </Grid>
                                </Grid>
                            </Button>
                        </NavLink>
                    </Grid>
                    <Grid item xs={2}>
                      <NavLink to='/Invoice/' style={{ textDecoration: 'none' }}>
                        <Button
                            variant="contained"
                            sx={{
                                width: '150px', mt: 3, mb: 2, background: '#5D5FEF', borderRadius: '8px', textTransform: 'none'
                            }}
                        >
                            <Grid container justify="center">
                                <Grid item xs={2}>
                                    <ArrowForwardIcon sx={{ color: 'white' }} />
                                </Grid>
                                <Grid item xs={10}>
                                    <Typography sx={{ fontFamily: 'Poppins' }}>
                                        Buka Invoice
                                    </Typography>
                                </Grid>
                            </Grid>
                        </Button>
                      </NavLink>
                    </Grid>
                </Grid>
            </Container>

        </div>
    )
}

export default ContainerSuccessPurchase

