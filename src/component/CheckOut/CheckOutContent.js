import { CheckBox } from '@mui/icons-material'
import { Alert, Button, Checkbox, CircularProgress, Container, Dialog, DialogContent, DialogTitle, FormControlLabel, Grid, List, ListItemButton, ListItemIcon, ListItemText, Paper, Snackbar, Table, TableBody, TableCell, tableCellClasses, TableContainer, TableHead, TableRow, Typography } from '@mui/material'
import { Box } from '@mui/system'
import React, { useEffect, useState } from 'react'
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import { APIRequest as Axios } from '../Axios.js';
import { NavLink, useNavigate } from 'react-router-dom';
import jwtDecode from 'jwt-decode';
import styled from '@emotion/styled';
import ShoppingCartCheckoutIcon from '@mui/icons-material/ShoppingCartCheckout';

const CheckOutContent = () => {
  const navigate = useNavigate();

  const getPayloadFromToken = () => {
    const token = localStorage.getItem('token');
    try {
      const decoded = jwtDecode(token);
      return decoded;
    } catch (err) {
      return null;
    }
  };
  const token = getPayloadFromToken();
  const accessToken = localStorage.getItem('token');
  const headers = { Authorization: `Bearer ${accessToken}` };
  const [email, setEmail] = useState("");

  const [basketList, setBasketList] = useState([]);
  const [listPaymentMethod, setListPaymentMethod] = useState([]);
  const [checkedItemList, setCheckedItemList] = useState([]);

  const [totalHarga, setTotalHarga] = useState(0);
  const [checked, setChecked] = useState([]);
  const [checkAll, setCheckAll] = useState(false);

  const [activeButton, setActiveButton] = useState(false);

  const [finishLoad, setFinishLoad] = useState(false);

  useEffect(() => {
    const setEmailValue = async () => {
      if (token !== null) {
        setEmail(token.email);
      } else {
        navigate("/Login");
      }
    };
    setEmailValue();
  }, []);

  const fetchBasket = async () => {
  if (email !== "") {
    try {
      const result = await Axios.get('Basket/SelectAll?email=' + email, { headers });
      setBasketList(result.data);
      setFinishLoad(true);
    } catch (error) {
      handleOpenSnackbar();
    }
    }
  };

  const fetchPaymentMethod = async () => {
    if (email != "") {
      try{
        const result = await Axios.get('PaymentMethod/SelectAll', {headers});
        setListPaymentMethod(result.data);
      }catch(error){
        handleOpenSnackbar();
      }
    }
  };

  useEffect(() => {
    fetchBasket();
    fetchPaymentMethod();
  }, [email])

  useEffect(() => {
    const setCheckBox = () => {
      for (let x = 0; x < basketList.length; x++) {
        setChecked(currentDateList => [...currentDateList, false]);
      };
    }
    setCheckBox();
  }, [basketList]);

  useEffect(() => {
    if (checked.every((check) => check == true)) {
      setCheckAll(true);
    } else {
      setCheckAll(false);
    }
  }, [checked])

  useEffect(() => {
    if (totalHarga === 0) {
      setActiveButton(true);
    } else {
      setActiveButton(false);
    }
  }, [totalHarga])

  const handleCheckAll = () => {
    setTotalHarga(0);
    const newChecked = checked.map((item, index) => {
      item = false;
      return item;
    })
    setChecked(newChecked);
    if (!checkAll) {
      setChecked(checked.map((item) => true));
      setTotalHarga(basketList.reduce((total, item) => {
        return total + item.harga;
      }, 0));
    }
  }

  const handleCheckboxChange = (id) => {
    const newChecked = checked.map((item, index) => {
      if (id == index) {
        item = !item;
        basketList.map((itemBasket, indexBasket) => {
          if (id == indexBasket) {
            if (item) {
              setTotalHarga(totalHarga + itemBasket.harga);
            } else {
              setTotalHarga(totalHarga - itemBasket.harga);
            }
          }
        })
      }
      return item;
    })
    setChecked(newChecked);
  };

  const deleteBasketList = async (id,index) => {
    basketList.map((item,indexCheck) => {
      if(indexCheck==index){
        setTotalHarga(totalHarga - item.harga);
      }
    });
    const newChecked = checked.filter((item,indexCheck) => index !== indexCheck);
    setChecked(newChecked);
    Axios({
      method: "Delete",
      url: "Basket/Delete?id=" + id,
      headers: {
        "Content-Type": "multipart/form-data",
        "Authorization": `Bearer ${accessToken}`
      }
    })
      .then((res) => {
        if (res.status === 200) {
          fetchBasket();
        }
      })
      .catch((err) => {
        handleOpenSnackbar();
      })
  }

  const [open, setOpen] = useState(false);

  const showPayment = () => {
    fetchPaymentMethod();
    setOpen(true);
  };

  const closePayment = () => {
    setOpen(false);
  };

  const [selectedIndex, setSelectedIndex] = useState(1);

  const handleListItemClick = (event, index) => {
    setSelectedIndex(index);
  };

  const checkedItems = () => {
    basketList.map((item, index) => {
      if (checked[index] === true) {
        setCheckedItemList(currentItemList => [...currentItemList, item.id]);
      }
    })
  }

  useEffect(() => {
    const insert = async () => {
      if (totalHarga != 0) {
        const data = {
          Id: checkedItemList,
          TotalHarga: totalHarga,
          Email: email
        };
        try {
          let response = await Axios.post("Invoice/Insert", data,{headers});
          navigate("/PurchaseSuccess");
        } catch (error) {
          handleOpenSnackbar();
        }
      }
    }
    insert();
  }, [checkedItemList])

  const saveToDb = async () => {
    checkedItems();
  }

  const [openSnackbar, setOpenSnackbar] = useState(false);
  const handleOpenSnackbar = () =>{
    setOpenSnackbar(true);
  }
  const handleCloseSnackbar = () =>{
    setOpenSnackbar(false);
  }

  const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
      color: 'gray',
    },
    [`&.${tableCellClasses.body}`]: {
      fontSize: 14,
    },
  }));

  const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(even)': {
    },
    '&:last-child td, &:last-child th': {
      border: 0,
    },
  }));


  if (basketList.length != 0 && checked.length != 0) {
    return (
      <Box>
        <TableContainer sx={{ width: '90%', margin: 'auto' }}>
          <Table sx={{ minWidth: 700, borderBottom: '1px solid rgba(224, 224, 224, 1)' }} aria-label="customized table">
            <TableHead>
              <TableRow>
                <StyledTableCell align="right">
                  <Checkbox checked={checkAll} onClick={handleCheckAll} />
                </StyledTableCell>
                <StyledTableCell align="left">Pilih Semua</StyledTableCell>
                <StyledTableCell></StyledTableCell>
                <StyledTableCell></StyledTableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {basketList.map((item, index) => (
                <StyledTableRow key={index}>
                  <StyledTableCell component="th" scope="row" align="right" width={20}>
                    <Checkbox sx={{ color: '#5D5FEF' }} checked={checked[index]} onChange={() => handleCheckboxChange(index)} />
                  </StyledTableCell>
                  <StyledTableCell align="left" width={200}><img src={item.gambar} alt="Image not Found" style={{ maxHeight: '200px', maxWidth: '200px' }} /></StyledTableCell>
                  <StyledTableCell align="left">
                    <Typography variant="h8" sx={{ color: 'gray' }}>
                      {item.kategoriName}
                    </Typography>
                    <Typography variant="h6">
                      {item.kelasName}
                    </Typography>
                    <Typography variant="h8">
                      Jadwal : {new Date(item.jadwal).toLocaleDateString("id-ID", { weekday: "long", year: "numeric", month: "long", day: "numeric" })}
                    </Typography>
                    <br />
                    <Typography variant="h6" sx={{ color: '#5D5FEF', marginTop: 2, marginBottom: 2 }}>
                      IDR {item.harga.toLocaleString("de-DE")}
                    </Typography></StyledTableCell>
                  <StyledTableCell align="center">
                    <Button sx={{ color: 'black' }} onClick={() => deleteBasketList(item.id,index)}>
                      <DeleteForeverIcon sx={{ color: '#EB5757' }} /> Delete
                    </Button></StyledTableCell>
                </StyledTableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
        <Box sx={{ borderTop: '3px solid rgba(224, 224, 224, 1)', marginTop: 10, paddingTop: 5 }}>
        </Box>
        <Box sx={{ position: 'relative', bottom: 0, left: 0, right: 0, width: '90%', margin: 'auto', marginBottom: 5 }}>
          <Box sx={{ alignItems: 'center', justifyContent: 'center', gap: 5 }}>
            <Box sx={{ textAlign: 'left' }}>
              <Typography variant='h7' display="inline">
                Total Biaya
              </Typography>
              <Typography variant='h6' sx={{ marginLeft: 5, color: '#5D5FEF' }} display="inline">
                IDR {totalHarga.toLocaleString("de-DE")}
              </Typography>
              <Button variant='contained' disabled={activeButton} sx={{ width: 250, backgroundColor: '#5D5FEF', color: 'white', borderRadius: '8px', display: 'inline', float: 'right' }} onClick={showPayment}>
                Bayar Sekarang
              </Button>
            </Box>
          </Box>
        </Box>
        {/* CheckOut Confirmation Dialog Box */}
        <Dialog open={open} onClose={closePayment}>
          <Box sx={{ alignItems: 'center' }}>
            <DialogTitle sx={{ textAlign: 'center' }} variant="h6" component="h2">
              Pilih Metode Pembayaran
            </DialogTitle>
            <DialogContent sx={{ textAlign: 'center' }}>
              <Grid container spacing={2}>
                <Grid item xs={12}>
                  <List component="nav">
                    {listPaymentMethod.map((item, index) => (
                      <ListItemButton
                        key={index}
                        selected={selectedIndex === index}
                        onClick={(event) => handleListItemClick(event, index)}
                      >
                        <ListItemIcon sx={{ width: 40, height: 40 }}>
                          <img src={item.imageFile} alt="Image not Found" style={{ maxHeight: '100%', maxWidth: '100%', objectFit: 'contain' }} />
                        </ListItemIcon>
                        <ListItemText primary={item.name} />
                      </ListItemButton>
                    ))}
                  </List>
                </Grid>
                <Grid item xs={12}>
                  <Button sx={{ width: 150, backgroundColor: 'white', color: '#5D5FEF', border: '1px solid #5D5FEF', borderRadius: '8px', padding: '10px 20px 10px 20px', marginRight: 1 }} onClick={closePayment}>Batal</Button>
                  <Button variant='contained' sx={{ width: 150, backgroundColor: '#5D5FEF', color: 'white', borderRadius: '8px', padding: '5px 10px 5px 10px' }} onClick={saveToDb}>
                      Bayar
                  </Button>
                </Grid>
              </Grid>
            </DialogContent>
          </Box>
        </Dialog>
        <Snackbar open={openSnackbar} autoHideDuration={6000} onClose={handleCloseSnackbar}>
          <Alert onClose={handleCloseSnackbar} severity="error" sx={{ width: '100%' }}>
              Unauthorize Access!
          </Alert>
        </Snackbar>
      </Box>
    )
  } else if (basketList.length == 0 && finishLoad == false) {
    return (
      <Container component="main" maxWidth="md" sx={{ height: "499px", justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
        <CircularProgress color="secondary" size={100} />
      </Container>
    )
  } else {
    return (
      <Container component="main" maxWidth="md" sx={{ height: "499px", justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
        <Typography variant='h2' style={{ fontWeight: 'bold', color: '#ff5722', textShadow: '1px 1px #ddd' }}>
          Keranjang Anda kosong!
        </Typography>
        <Typography variant='h4' style={{ fontWeight: 'bold', textShadow: '1px 1px #ddd', marginTop: '10px' }}>
          Isi dengan semua kelas yang Anda sukai!
        </Typography>
        <ShoppingCartCheckoutIcon sx={{ fontSize: 200, color: '#ff5722', marginTop: '20px' }} />
      </Container>
    )
  }
}

export default CheckOutContent
