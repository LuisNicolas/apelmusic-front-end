import React from 'react'
import { Box, Container } from '@mui/system'
import { colors, Typography } from '@mui/material'
import { NavLink } from 'react-router-dom'
import { Grid } from '@mui/material'
import Button from '@mui/material/Button'
import EmailPurchase from '../Images/EmailPurchase.PNG'
import { useParams } from 'react-router-dom'
import { APIRequest as Axios } from '../component/Axios.js';
import { useEffect } from 'react'


function ContainerKonfirmasiEmailSukses() {
    const { email } = useParams();
    console.log(email);

    // axios.post(`/api/Register/ActivateAccount?email=${email}`)
    // .then(function (response) {
    //   console.log(response);
    // })
    // .catch(function (error) {
    //   console.log(error);
    // });
    useEffect(() => {
        Axios({
            method: "POST",
            url: `/Register/ActivateAccount?email=${email}`,
            headers: {
                "Content-Type": "application/json"
            }
        })
        .then((res) => {
            if (res.status === 200) {
                console.log("Activation Success");
            }
        })
        .catch((err) => {
            console.log(err);
        })
    }, [email]);

    return (
        <div>
            <Container maxWidth="s" sx={{ marginTop: '122px' }}>
                <Box>
                    <img src={EmailPurchase} style={{ width: '250px', height: '250px' }} />
                </Box>
                <Typography variant="h6" sx={{ fontSize: '24px', fontWeight: 700, marginTop: '20px', color: '#5D5FEF' }}>
                    Konfirmasi Email Berhasil
                </Typography>
                <Typography variant="h6" sx={{ fontSize: '16px', fontWeight: 400, marginTop: '20px' }}>
                    Silahkan Login terlebih dahulu untuk masuk ke aplikasi
                </Typography>
                <NavLink to="/Login" style={{ textDecoration: 'none' }}>
                    <Button variant="contained" sx={{ width: '176px', marginTop: '20px', backgroundColor: '#5D5FEF', color: 'white', fontWeight: 700, fontSize: '16px' }}>
                        Login
                    </Button>
                </NavLink>
            </Container>

        </div>
    )
}

export default ContainerKonfirmasiEmailSukses

