import * as React from 'react';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { NavLink } from 'react-router-dom';
import { APIRequest as Axios } from '../component/Axios.js';
import { Snackbar } from '@mui/material';
import MuiAlert from '@mui/material/Alert';

const theme = createTheme();

export default function ResetPasswordEmail() {
    //useEffectPasswordConfirmation

    const [open, setOpen] = React.useState(false);
    const [severity, setSeverity] = React.useState('success');
    const [message, setMessage] = React.useState('');

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpen(false);
    };

    const handleSubmit = (event) => {
        event.preventDefault();
        const data = new FormData(event.currentTarget);
        const email = data.get('email');
        Axios({
            method: "POST",
            url: `/Register/ResetPasswordEmail?email=${email}`,
            headers: {
                "Content-Type": "application/json"
            }
        })
            .then((res) => {
                if (res.status === 200) {
                    setSeverity('success');
                    setMessage('Request reset password diterima, Silahkan cek email anda untuk melakukan reset password');
                }
            })
            .catch((err) => {
                console.log(err);
                setSeverity('error');
                setMessage('Email tidak terdaftar');
            })
        //refresh();

        console.log({
            email: data.get('email')
        });
        setOpen(true);
    }

    return (
        <ThemeProvider theme={theme}>
            <Container component="main" maxWidth="md">
                <CssBaseline />
                <Box
                    sx={{
                        marginTop: 20

                    }}
                >

                    <Grid container justify="flex-start">
                        <Grid>
                            <Typography component="h1" variant="h1" align='left' sx={{ fontSize: '24px' }}>
                                Reset Password
                            </Typography>
                            <Grid>
                                <Typography component="h2" variant="h2" align='left' sx={{ fontSize: '16px' }}>
                                    Silahkan masukkan terlebih dahulu email anda
                                </Typography>
                            </Grid>
                        </Grid>

                    </Grid>
                    <Box component="form" onSubmit={handleSubmit} sx={{ mt: 1 }}>
                        <TextField
                            margin="normal"
                            fullWidth
                            required
                            name="email"
                            label="Masukkan Email"
                            type="email"
                            id="email"
                            autoComplete="email"
                        />
                        <Grid container justify="center">
                            <Grid item xs={6} md={1}>
                                <NavLink to='/Login' style={{ textDecoration: 'none' }}>
                                    <Button
                                        sx={{
                                            width: '150px',
                                            mt: 3,
                                            mb: 2,
                                            background: 'white',
                                            border: '2px solid #5D5FEF',
                                            borderRadius: '8px',
                                            color: '#5D5FEF',
                                            textTransform: 'none'
                                        }}
                                    >
                                        Batal
                                    </Button>
                                </NavLink>
                            </Grid>
                            <Grid item xs={6}>

                                <Button
                                    type="submit"
                                    variant="contained"
                                    sx={{
                                        width: '150px', mt: 3, mb: 2, background: '#5D5FEF', borderRadius: '8px', textTransform: 'none'
                                    }}
                                >
                                    Konfirmasi
                                </Button>

                            </Grid>
                        </Grid>
                    </Box>
                </Box>
            </Container>
            <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
                <MuiAlert elevation={6} variant="filled" onClose={handleClose} severity={severity}>
                    {message}
                </MuiAlert>
            </Snackbar>
        </ThemeProvider>
    );
}