import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import AppleIcon from '@mui/icons-material/Apple';
import { NavLink } from 'react-router-dom';
import { Grid, Hidden } from '@mui/material';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import { useEffect } from 'react';
import { useContext } from 'react';
import { AuthContext } from '../Context/AuthContext';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import LogoutIcon from '@mui/icons-material/Logout';
import jwtDecode from 'jwt-decode';
import AddCardIcon from '@mui/icons-material/AddCard';


export default function NavbarLogin() {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const { isLogin, setIsLogin } = useContext(AuthContext);
  const [role, setRole] = React.useState(null);
  useEffect(() => {
    const token = localStorage.getItem('token');
    if (token) {
      setIsLogin(true);
      setRole(getPayloadFromToken().role);
    }
  }, []);

  const getPayloadFromToken = () => {
    const token = localStorage.getItem('token');
    try {
      const decoded = jwtDecode(token);
      return decoded;
    } catch (err) {
      return null;
    }
  };
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  useEffect(() => {
    setAnchorEl(null);
  }, []);

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar sx={{ background: "#F2C94C" }}>

          <NavLink to='/' style={{ textDecoration: 'none' }}>
            <Grid container>
              <Grid>
                <AppleIcon sx={{ mr: 1, color: "black", marginTop: '6px', marginBottom: '1px' }} />
              </Grid>
              <Grid>
                <Typography
                  variant="h6"
                  sx={{
                    mr: 1,
                    fontFamily: 'monospace',
                    fontWeight: 1000,
                    color: 'black',
                    textDecoration: 'none',
                    marginRight: "auto",
                    marginTop: '5px', marginBottom: '1px'
                  }}
                >MUSIC</Typography>
              </Grid>
            </Grid>
          </NavLink>
          {!isLogin ? (
            <>
              <Hidden mdDown>
                <NavLink to='/register' style={{ mr: 1, fontWeight: 620, size: '10px', marginLeft: "auto", color: "black", fontSize: '16px', lineHeight: '24px', textDecoration: 'none' }}>Daftar Sekarang</NavLink>
                <NavLink to='/login' style={{ textDecoration: 'none' }}>
                  <Button variant="contained" sx={{ fontWeight: 520, color: 'white', background: '#5D5FEF', borderRadius: '8px', marginLeft: "50px", textTransform: 'none' }}>
                    Masuk</Button>
                </NavLink>
              </Hidden>
              <Hidden mdUp>
                <IconButton aria-label="menu" sx={{ marginLeft: 'Auto' }} onClick={handleClick}>
                  <MenuIcon aria-label="menu" />
                  <Menu
                    id="simple-menu"
                    anchorEl={anchorEl}
                    keepMounted
                    open={Boolean(anchorEl)}
                    onClose={handleClose}
                  >
                    <MenuItem onClick={handleClose}><NavLink to='/register' style={{ mr: 1, fontWeight: 620, size: '10px', color: "black", fontSize: '16px', lineHeight: '24px', textDecoration: 'none' }}>Daftar Sekarang</NavLink></MenuItem>
                    <MenuItem onClick={handleClose}><NavLink to='/login' style={{ mr: 1, fontWeight: 620, size: '10px', color: "black", fontSize: '16px', lineHeight: '24px', textDecoration: 'none' }}>Masuk</NavLink></MenuItem>
                  </Menu>
                </IconButton>
              </Hidden>
            </>
          ) : (
            <>
            {role === "1" ? (
              <>
              <Hidden mdDown>
                <NavLink to='/Checkout' style={{ marginLeft: 'auto', textDecoration: 'none' }}>
                  <ShoppingCartIcon sx={{ color: 'black' }} />
                </NavLink>
                <NavLink to='/MyClass' style={{ textDecoration: 'none' }}>
                  <Typography sx={{ mr: 1, fontWeight: 620, size: '10px', marginLeft: "20px", color: "black", fontSize: '16px', lineHeight: '24px', textDecoration: 'none' }}>Kelasku</Typography>
                </NavLink>
                <NavLink to='/Invoice' style={{ textDecoration: 'none' }}>
                  <Typography sx={{ mr: 1, fontWeight: 620, size: '10px', marginLeft: "20px", color: "black", fontSize: '16px', lineHeight: '24px', textDecoration: 'none' }}>Pembelian</Typography>
                </NavLink>
                <NavLink to='/' style={{ textDecoration: 'none' }}>
                  <LogoutIcon sx={{ marginLeft: '30px', color: 'black' }} onClick={() => {
                    localStorage.removeItem('token');
                    setIsLogin(false);
                  }} />
                </NavLink>
              </Hidden>
              <Hidden mdUp>
                <IconButton aria-label="menu" sx={{ marginLeft: 'Auto' }} onClick={handleClick}>
                  <MenuIcon aria-label="menu" />
                  <Menu
                    id="simple-menu"
                    anchorEl={anchorEl}
                    keepMounted
                    open={Boolean(anchorEl)}
                    onClose={handleClose}
                  >
                    <MenuItem onClick={handleClose}><NavLink to='/Checkout' style={{ mr: 1, fontWeight: 620, size: '10px', color: "black", fontSize: '16px', lineHeight: '24px', textDecoration: 'none' }}>Keranjang</NavLink></MenuItem>
                    <MenuItem onClick={handleClose}><NavLink to='/Myclass' style={{ mr: 1, fontWeight: 620, size: '10px', color: "black", fontSize: '16px', lineHeight: '24px', textDecoration: 'none' }}>Kelasku</NavLink></MenuItem>
                    <MenuItem onClick={handleClose}><NavLink to='/Invoice' style={{ mr: 1, fontWeight: 620, size: '10px', color: "black", fontSize: '16px', lineHeight: '24px', textDecoration: 'none' }}>Pembelian</NavLink></MenuItem>
                    <MenuItem onClick={() => {
                      localStorage.removeItem('token');
                      setIsLogin(false);
                      handleClose();
                    }}><NavLink to='/' style={{ mr: 1, fontWeight: 620, size: '10px', color: "black", fontSize: '16px', lineHeight: '24px', textDecoration: 'none' }} >Logout</NavLink></MenuItem>
                  </Menu>
                </IconButton>
              </Hidden>
            </>
            ) : (
              <>
                <Hidden mdDown>
                <NavLink to='/FormMetodePembayaran' style={{ marginLeft: 'auto', textDecoration: 'none' }}>
                  <AddCardIcon sx={{ color: 'black' }} />
                </NavLink>
                
                <NavLink to='/Invoice' style={{ textDecoration: 'none' }}>
                  <Typography sx={{ mr: 1, fontWeight: 620, size: '10px', marginLeft: "20px", color: "black", fontSize: '16px', lineHeight: '24px', textDecoration: 'none' }}>Pembelian</Typography>
                </NavLink>
                <NavLink to='/' style={{ textDecoration: 'none' }}>
                  <LogoutIcon sx={{ marginLeft: '30px', color: 'black' }} onClick={() => {
                    localStorage.removeItem('token');
                    setIsLogin(false);
                  }} />
                </NavLink>
              </Hidden>
              <Hidden mdUp>
                <IconButton aria-label="menu" sx={{ marginLeft: 'Auto' }} onClick={handleClick}>
                  <MenuIcon aria-label="menu" />
                  <Menu
                    id="simple-menu"
                    anchorEl={anchorEl}
                    keepMounted
                    open={Boolean(anchorEl)}
                    onClose={handleClose}
                  >
                    <MenuItem onClick={handleClose}><NavLink to='/FormMetodePembayaran' style={{ mr: 1, fontWeight: 620, size: '10px', color: "black", fontSize: '16px', lineHeight: '24px', textDecoration: 'none' }}>Dashboard Payment</NavLink></MenuItem>
                    <MenuItem onClick={handleClose}><NavLink to='/Invoice' style={{ mr: 1, fontWeight: 620, size: '10px', color: "black", fontSize: '16px', lineHeight: '24px', textDecoration: 'none' }}>Pembelian</NavLink></MenuItem>
                    <MenuItem onClick={() => {
                      localStorage.removeItem('token');
                      setIsLogin(false);
                      handleClose();
                    }}><NavLink to='/' style={{ mr: 1, fontWeight: 620, size: '10px', color: "black", fontSize: '16px', lineHeight: '24px', textDecoration: 'none' }} >Logout</NavLink></MenuItem>
                  </Menu>
                </IconButton>
              </Hidden>
              </>
            )}
          </>
           
          )}
        </Toolbar>
      </AppBar>
    </Box>
  );
}
