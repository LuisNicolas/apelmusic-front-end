import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';

import AppleIcon from '@mui/icons-material/Apple';
import { NavLink } from 'react-router-dom';
import { Grid, Hidden } from '@mui/material';
import { useEffect } from 'react';

export default function NavbarLogin() {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  useEffect(() => {
    setAnchorEl(null);
  }, []);

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static" elevation={0}>
        <Toolbar sx={{ background: "white" }}>
          <NavLink to='/' style={{ textDecoration: 'none' }}>
            <Grid container>
              <Grid>
                <AppleIcon sx={{ mr: 1, color: "black", marginTop: '6px', marginBottom: '1px' }} />
              </Grid>
              <Grid>
                <Typography
                  variant="h6"
                  sx={{
                    mr: 1,
                    fontFamily: 'monospace',
                    fontWeight: 1000,
                    color: 'black',
                    textDecoration: 'none',
                    marginRight: "auto",
                    marginTop: '5px', marginBottom: '1px'
                  }}
                >MUSIC</Typography>
              </Grid>
            </Grid>
          </NavLink>

        </Toolbar>
      </AppBar>
    </Box>
  );
}
