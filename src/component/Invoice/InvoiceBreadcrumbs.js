import { Breadcrumbs, Typography } from '@mui/material'
import React from 'react'
import { NavLink } from 'react-router-dom'

const InvoiceBreadcrumbs = () => {
  return (
    <div style={{margin:'30px 0px 30px 0px'}}>
        <Breadcrumbs separator="›" aria-label="breadcrumb">
            <NavLink to='/' color="gray" style={{textDecoration:'none',color:'gray'}}>
                Beranda
            </NavLink>,
            <Typography color="#5D5FEF">
                Invoice
            </Typography>
        </Breadcrumbs>
    </div>
  )
}

export default InvoiceBreadcrumbs
