import * as React from 'react';
import { styled } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { Box } from '@mui/system';
import jwtDecode from 'jwt-decode';
import { useState } from 'react';
import { useEffect } from 'react';
import { APIRequest as Axios } from '../Axios.js';
import { Alert, Button, CircularProgress, Container, Snackbar, Typography } from '@mui/material';
import { NavLink, useNavigate } from 'react-router-dom';
import RedeemIcon from '@mui/icons-material/Redeem';

export default function InvoiceTable() {

  const [listInvoice, setListInvoice] = useState([]);
  const [finishLoad, setFinishLoad] = useState(false);

  const navigate = useNavigate();

  const getPayloadFromToken = () => {
    const token = localStorage.getItem('token');
    try {
      const decoded = jwtDecode(token);
      return decoded;
    } catch (err) {
      return null;
    }
  };
  const token = getPayloadFromToken();
  const accessToken = localStorage.getItem('token');
  const headers = { Authorization: `Bearer ${accessToken}` };
  const [email, setEmail] = useState("");

  useEffect(() => {
    const setEmailValue = async () => {
      if (token !== null) {
        setEmail(token.email);
      } else {
        navigate("/Login");
      }
    };
    setEmailValue();
  }, []);

  useEffect(() => {
    const fetchInvoice = async () => {
      if (email !== "") {
        try{
          const result = await Axios.get('Invoice/SelectAll?email=' + email,{headers});
        setListInvoice(result.data);
        setFinishLoad(true);
        }catch(err){
          handleOpenSnackbar();
        }
      }
    };
    fetchInvoice();
  }, [email]);

  const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
      backgroundColor: '#F2C94C',
      color: '#4F4F4F',
    },
    [`&.${tableCellClasses.body}`]: {
      fontSize: 14,
    },
  }));

  const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(even)': {
      backgroundColor: '#F2C94C33',
    },
    '&:last-child td, &:last-child th': {
      border: 0,
    },
  }));
  const [openSnackbar, setOpenSnackbar] = useState(false);
  const handleOpenSnackbar = () =>{
    setOpenSnackbar(true);
  }
  const handleCloseSnackbar = () =>{
    setOpenSnackbar(false);
  }

  if (listInvoice.length != 0) {
    return (
      <div>
        <Typography variant='h5' sx={{ textAlign: 'left', marginBottom: 3 }}>
          Menu Invoice
        </Typography>
        <Box>
          <TableContainer component={Paper}>
            <Table sx={{ minWidth: 700 }} aria-label="customized table">
              <TableHead>
                <TableRow>
                  <StyledTableCell align="center">No</StyledTableCell>
                  <StyledTableCell align="center">No.Invoice</StyledTableCell>
                  <StyledTableCell align="center">Tanggal Beli</StyledTableCell>
                  <StyledTableCell align="center">Jumlah Kursus</StyledTableCell>
                  <StyledTableCell align="center">Total Harga</StyledTableCell>
                  <StyledTableCell align="center">Action</StyledTableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {listInvoice.map((row, index) => (
                  <StyledTableRow key={index}>
                    <StyledTableCell component="th" scope="row" align="center">
                      {index + 1}
                    </StyledTableCell>
                    <StyledTableCell align="center">APM{('0000' + row.id).slice(-4)}</StyledTableCell>
                    <StyledTableCell align="center">{new Date(row.tglBeli).toLocaleDateString("id-ID", { year: "numeric", month: "long", day: "numeric" })}</StyledTableCell>
                    <StyledTableCell align="center">{row.jumlahKursus}</StyledTableCell>
                    <StyledTableCell align="center">IDR {row.totalHarga.toLocaleString("de-DE")}</StyledTableCell>
                    <StyledTableCell align="center">
                      <Button variant='contained' sx={{ width: '100%', backgroundColor: '#5D5FEF', color: 'white', borderRadius: '8px' }}>
                        <NavLink style={{ textDecoration: 'none', color: 'white', height: '100%', width: '100%', padding: '5px 10px 5px 10px' }} to={{ pathname: "/DetailInvoice/" + row.id }}>
                          Rincian
                        </NavLink>
                      </Button>
                    </StyledTableCell>
                  </StyledTableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Box>
        <Snackbar open={openSnackbar} autoHideDuration={6000} onClose={handleCloseSnackbar}>
          <Alert onClose={handleCloseSnackbar} severity="error" sx={{ width: '100%' }}>
              Unauthorize Access!
          </Alert>
        </Snackbar>
      </div>
    )
  } else if (listInvoice.length == 0 && finishLoad == false) {
    return (
      <Container component="main" maxWidth="md" sx={{ height: "499px", justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
        <CircularProgress color="secondary" size={100} />
      </Container>
    )
  } else {
    return (
      <Container component="main" maxWidth="md" sx={{ height: "499px", justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
        <Typography variant='h2' style={{ fontWeight: 'bold', color: '#ff5722', textShadow: '1px 1px #ddd' }}>
          Anda belum melakukan pembelian!
        </Typography>
        <Typography variant='h4' style={{ fontWeight: 'bold', textShadow: '1px 1px #ddd', marginTop: '10px' }}>
          Ayo wujudkan sesuatu yang luar biasa! Mulai belanja sekarang!
        </Typography>
        <RedeemIcon sx={{ fontSize: 100, color: '#ff5722', marginTop: '20px' }} />
      </Container>

    )
  }
}

