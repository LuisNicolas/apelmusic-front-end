import { CheckBox } from '@mui/icons-material'
import { Alert, Button, Checkbox, CircularProgress, Dialog, DialogContent, DialogTitle, FormControlLabel, Grid, List, ListItemButton, ListItemIcon, ListItemText, Snackbar, Table, TableBody, TableCell, tableCellClasses, TableContainer, TableRow, Typography } from '@mui/material'
import { Box, Container } from '@mui/system'
import React, { useEffect, useState } from 'react'
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import { APIRequest as Axios } from '../Axios.js';
import { render } from 'react-dom';
import { NavLink, useNavigate } from 'react-router-dom';
import { Divider } from '@mui/material';
import jwtDecode from 'jwt-decode';
import Card from '@mui/material/Card';
import styled from '@emotion/styled';
import ShoppingBasketIcon from '@mui/icons-material/ShoppingBasket';

const MyClassContent = () => {

  const navigate = useNavigate();

  const getPayloadFromToken = () => {
    const token = localStorage.getItem('token');
    try {
      const decoded = jwtDecode(token);
      return decoded;
    } catch (err) {
      return null;
    }
  };
  const token = getPayloadFromToken();
  const accessToken = localStorage.getItem('token');
  const headers = { Authorization: `Bearer ${accessToken}` };
  const [email, setEmail] = useState("");

  useEffect(() => {
    const setEmailValue = async () => {
      if (token !== null) {
        setEmail(token.email);
      } else {
        navigate("/Login");
      }
    };
    setEmailValue();
  }, []);

  const [myClassList, setMyClassList] = useState([]);
  const [finishLoad, setFinishLoad] = useState(false);

  useEffect(() => {
    const fetchMyClass = async () => {
      if (email !== "") {
        try{
          const result = await Axios.get('MyClass/SelectClass?email=' + email,{headers});
        setMyClassList(result.data);
        setFinishLoad(true);
        }catch(err){
          handleOpenSnackbar();
        }
      }
    };
    fetchMyClass();
  }, [email]);

  const [openSnackbar, setOpenSnackbar] = useState(false);
  const handleOpenSnackbar = () =>{
    setOpenSnackbar(true);
  }
  const handleCloseSnackbar = () =>{
    setOpenSnackbar(false);
  }

  const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
      color: 'gray',
    },
    [`&.${tableCellClasses.body}`]: {
      fontSize: 14,
    },
  }));

  const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(even)': {
    },
    '&:last-child td, &:last-child th': {
      border: 0,
    },
  }));

  if (myClassList.length != 0) {
    return (
      <Box>
        <TableContainer sx={{ width: '90%', margin: 'auto', marginBottom: 10, marginTop: 5 }}>
          <Table sx={{ minWidth: 700, borderBottom: '1px solid rgba(224, 224, 224, 1)' }} aria-label="customized table">
            <TableBody>
              {myClassList.map((item, index) => (
                <StyledTableRow key={index}>
                  <StyledTableCell align="right" sx={{ width: 200 }}><img src={item.gambar} alt="Image not Found" style={{ maxHeight: '200px', maxWidth: '200px' }} /></StyledTableCell>
                  <StyledTableCell align="left">
                    <Typography variant="h8" sx={{ color: 'gray' }}>
                      {item.kategori}
                    </Typography>
                    <Typography variant="h6">
                      {item.nama}
                    </Typography>
                    <Typography variant="h8" sx={{ color: '#5D5FEF' }}>
                      Jadwal : {new Date(item.jadwal).toLocaleDateString("id-ID", { weekday: "long", year: "numeric", month: "long", day: "numeric" })}
                    </Typography>
                  </StyledTableCell>
                </StyledTableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
        <Snackbar open={openSnackbar} autoHideDuration={6000} onClose={handleCloseSnackbar}>
          <Alert onClose={handleCloseSnackbar} severity="error" sx={{ width: '100%' }}>
              Unauthorize Access!
          </Alert>
        </Snackbar>
      </Box>
    )
  } else if (myClassList.length == 0 && finishLoad == false) {
    return (
      <Container component="main" maxWidth="md" sx={{ height: "499px", justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
        <CircularProgress color="secondary" size={100} />
      </Container>
    )
  } else {
    return (
      <Container component="main" maxWidth="lg" sx={{ height: "499px", justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
        <Typography variant='h3' style={{ fontWeight: 'bold', textShadow: '1px 1px #ddd' }}>
          Ups! Sepertinya Anda belum bergabung dengan kelas apa pun.
        </Typography>
        <Typography variant='h5' style={{ fontWeight: 'bold', textShadow: '1px 1px #ddd', marginTop: '10px' }}>
          Mari temukan kelas yang tepat untuk Anda. Mulai belanja sekarang!
        </Typography>
        <NavLink to="/" style={{ textDecoration: 'none' }}>
          <ShoppingBasketIcon style={{ fontSize: '200px', color: '#ff5722', marginTop: '20px' }} />
        </NavLink>
      </Container>
    )
  }
}

export default MyClassContent
