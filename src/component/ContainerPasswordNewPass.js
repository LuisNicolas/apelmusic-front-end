import * as React from 'react';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { NavLink, useNavigate } from 'react-router-dom';
import { APIRequest as Axios } from '../component/Axios.js';
import { useParams } from 'react-router-dom';

const theme = createTheme();

export default function ResetPasswordNewPass() {
    //useEffectPasswordConfirmation
    const [passwordError, setPasswordError] = React.useState(false);
    const [passwordLengthError, setPasswordLengthError] = React.useState(false);
    const [passwordUppercaseError, setPasswordUppercaseError] = React.useState(false);
    const [passwordSymbolError, setPasswordSymbolError] = React.useState(false);
    const { email } = useParams();

    const navigate = useNavigate();

    const handleSubmit = (event) => {
        event.preventDefault();
        const data = new FormData(event.currentTarget);
        const password = data.get('password');

        if (password.length < 8) {
            setPasswordLengthError(true);
            return;
        } else {
            setPasswordLengthError(false);
        }

        if (!/[A-Z]/.test(password)) {
            setPasswordUppercaseError(true);
            return;
        } else {
            setPasswordUppercaseError(false);
        }

        if (!/[!@#$%^&*()_+\-=[\]{};':"\\|,.<>/?]/.test(password)) {
            setPasswordSymbolError(true);
            return;
        } else {
            setPasswordSymbolError(false);
        }
        if (data.get('password') !== data.get('passwordConfirm')) {
            setPasswordError(true);
            return;
            //alert("Password dan Konfirmasi Password tidak sama, mohon cek kembali");
        } else {
            Axios({
                method: "PATCH",
                url: `/Register/UpdatePassword`,
                data: {
                    email: email,
                    password: data.get('password')
                },
                headers: {
                    "Content-Type": "application/json"
                }
            })
            .then((res) => {
                if (res.status === 200) {
                    navigate('/Login');
                }
            })
            .catch((err) => {
                console.log(err);
            })
            console.log({
                password: data.get('password'),
                passwordConfirm: data.get('passwordConfirm')
            });
            setPasswordError(false);
        };
    }

    return (
        <ThemeProvider theme={theme}>
            <Container component="main" maxWidth="md">
                <CssBaseline />
                <Box
                    sx={{
                        marginTop: 20

                    }}
                >

                    <Grid container justify="flex-start">
                        <Grid>
                            <Typography component="h1" variant="h1" align='left' sx={{ fontSize: '24px' }}>
                                Reset Password
                            </Typography>
                            <Grid>
                                <Typography component="h2" variant="h2" align='left' sx={{ fontSize: '16px' }}>
                                    Silahkan masukkan terlebih dahulu email anda
                                </Typography>
                            </Grid>
                        </Grid>

                    </Grid>
                    <Box component="form" onSubmit={handleSubmit} sx={{ mt: 1 }}>
                        <TextField
                            margin="normal"
                            required
                            fullWidth
                            name="password"
                            label="Masukkan Password"
                            type="password"
                            id="password"
                            error={passwordLengthError || passwordUppercaseError || passwordSymbolError}
                            helperText={
                                passwordLengthError
                                    ? "Password harus memiliki panjang minimal 8 karakter"
                                    : passwordUppercaseError
                                        ? "Password harus memiliki setidaknya 1 huruf besar"
                                        : passwordSymbolError
                                            ? "Password harus memiliki setidaknya 1 simbol (!@#$%^&*()_+-=[]{};':\"\\|,.<>/?)"
                                            : ""
                            }
                        />
                        <TextField
                            margin="normal"
                            required
                            fullWidth
                            name="passwordConfirm"
                            label="Konfirmasi Password"
                            type="password"
                            id="passwordConfirm"
                            error={passwordError}
                            helperText={passwordError ? "Password dan Konfirmasi Password tidak sama, mohon cek kembali" : ""}
                            
                        />
                        <Grid container justify="center">
                            <Grid item xs={6} md={1}>
                                <NavLink to='/Login' style={{ textDecoration: 'none' }}>
                                    <Button
                                        sx={{
                                            width: '150px',
                                            mt: 3,
                                            mb: 2,
                                            background: 'white',
                                            border: '2px solid #5D5FEF',
                                            borderRadius: '8px',
                                            color: '#5D5FEF',
                                            textTransform: 'none'
                                        }}
                                    >
                                        Batal
                                    </Button>
                                </NavLink>
                            </Grid>
                            <Grid item xs={6}>
                                <Button
                                    type="submit"
                                    variant="contained"
                                    sx={{
                                        width: '150px', mt: 3, mb: 2, background: '#5D5FEF', borderRadius: '8px', textTransform: 'none'
                                    }}
                                >
                                    Konfirmasi
                                </Button>
                            </Grid>
                        </Grid>
                    </Box>
                </Box>
            </Container>
        </ThemeProvider>
    );
}