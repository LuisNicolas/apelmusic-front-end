import * as React from 'react';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import { NavLink, useNavigate } from 'react-router-dom';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { APIRequest as Axios } from '../component/Axios.js';
import { AuthContext } from '../Context/AuthContext';
import { useContext } from 'react';
import { useEffect } from 'react';
import jwtDecode from 'jwt-decode';
import { Snackbar } from '@mui/material';
import MuiAlert from '@mui/material/Alert';

const theme = createTheme();

export default function SignIn() {
    const { isLogin, setIsLogin } = useContext(AuthContext);

    const [open, setOpen] = React.useState(false);
    const [severity, setSeverity] = React.useState('success');
    const [message, setMessage] = React.useState('');
    const navigate = useNavigate();

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpen(false);
    };

    useEffect(() => {
        const token = localStorage.getItem('token');
        if (token) {
            setIsLogin(true);
        }
    }, []);
    const getPayloadFromToken = () => {
        const token = localStorage.getItem('token');
        try {
            const decoded = jwtDecode(token);
            return decoded;
        } catch (err) {
            return null;
        }
    };
    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = new FormData(event.currentTarget);
        Axios({
            method: "POST",
            url: "Register/Login",
            data: {
                email: data.get('email'),
                password: data.get('password'),
            },
            headers: {
                "Content-Type": "application/json"
            }
        })
            .then((res) => {
                if (res.status === 200) {
                    localStorage.setItem("token", res.data.token);
                    const role = getPayloadFromToken().role;

                    if (role === "1") {
                        setIsLogin(true);
                        navigate('/');
                    } else if (role === "0") {
                        setIsLogin(true);
                        navigate('/FormMetodePembayaran');
                    }

                }
            })
            .catch((err) => {
                setSeverity('error');
                setMessage('Email atau password salah');
            });
        // console.log({
        //     email: data.get('email'),
        //     password: data.get('password'),
        // });
        setOpen(true);
    };


    return (
        <ThemeProvider theme={theme}>
            <Container component="main" maxWidth="md">
                <CssBaseline />
                <Box
                    sx={{
                        marginTop: 20,
                    }}
                >

                    <Grid container justify="flex-start">
                        <Grid>
                            <Typography component="h1" variant="h1" align='left' sx={{ fontSize: '24px' }}>
                                Selamat Datang Musikers!
                            </Typography>
                        </Grid>
                        <Grid xs={12}>
                            <Typography component="h2" variant="h2" align='left' sx={{ fontSize: '16px' }}>
                                Login Dulu Yuk
                            </Typography>
                        </Grid>
                    </Grid>
                    <Box component="form" onSubmit={handleSubmit} sx={{ mt: 1 }}>
                        <TextField
                            margin="normal"
                            required
                            fullWidth
                            id="email"
                            label="Masukkan Email"
                            name="email"
                            autoComplete="email"
                            autoFocus
                        />
                        <TextField
                            margin="normal"
                            required
                            fullWidth
                            name="password"
                            label="Masukkan Password"
                            type="password"
                            id="password"
                            autoComplete="current-password"
                        />
                        <Grid container>
                            <Grid item xs>

                            </Grid>
                            <Grid item xs={12} md={2}>
                                <NavLink to='/ResetPasswordEmail' variant="body2" style={{ color: 'black', textDecoration: 'none' }}>
                                    Lupa kata sandi
                                </NavLink>
                            </Grid>
                        </Grid>
                        <Grid container justify="flex-start">
                            <Grid item md={1} xs={12}>
                                <Button
                                    type="submit"
                                    variant="contained"

                                    sx={{
                                        width: '150px', mt: 3, mb: 2, background: '#5D5FEF', borderRadius: '8px', textTransform: 'none'

                                    }}
                                >
                                    Masuk
                                </Button>
                            </Grid>
                            <Grid item xs={4} md={12}>
                                <Typography align='left'>Belum punya akun?
                                    <NavLink to='/register' style={{ textDecoration: 'none' }}>Daftar disini</NavLink>
                                </Typography>
                            </Grid>
                        </Grid>
                    </Box>
                </Box>
            </Container>
            <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
                <MuiAlert elevation={6} variant="filled" onClose={handleClose} severity={severity}>
                    {message}
                </MuiAlert>
            </Snackbar>
        </ThemeProvider>
    );
}