import styled from '@emotion/styled';
import { Skeleton, Typography } from '@mui/material';
import React, { useState } from 'react'

const CategoryInfo = (props) => {
   const pageData = props.pageData;

    const SectionTitle = styled(Typography)(({theme})=>({
        fontWeight: 650,
        fontSize:24,
        color: 'black',
        textDecoration: 'none',
        paddingTop: '20px',
        textAlign:'left'
      }));
      const SectionDescription = styled(Typography)(({theme})=>({
        fontSize:17,
        color: 'black',
        textDecoration: 'none',
        marginRight: "auto",
        paddingTop: '20px', 
        paddingBottom: '45px',
        textAlign:'left'
      }));

  if(pageData.gambarLaman!=undefined){
    return (
      <div style={{borderBottom:'2px solid #E0E0E0'}}>
        <img src={pageData.gambarLaman} alt="Image not found" style={{width:'100%'}}/>
        <div style={{margin:'auto',width:'90%'}}>
          <SectionTitle>{pageData.judulLaman}</SectionTitle>
          <SectionDescription>{pageData.deskripsiLaman}</SectionDescription>
        </div>
      </div>
    )
  }else{
    return (
      <div style={{borderBottom:'2px solid #E0E0E0'}}>
        <Skeleton variant='' height={396} width={1519} />
        <div style={{margin:'auto',width:'90%'}}>
          <SectionTitle><Skeleton variant='' height={50} width={1367} /></SectionTitle>
          <SectionDescription><Skeleton variant='' height={141} width={1367} /></SectionDescription>
        </div>
      </div>
    )
  }

}

export default CategoryInfo
