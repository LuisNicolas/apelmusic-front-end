import Axios from "axios"

export const APIRequest = Axios.create({
    // baseURL : "https://localhost:44394/api/"
    baseURL : process.env.REACT_APP_API_BASE_URL
});

export default APIRequest;